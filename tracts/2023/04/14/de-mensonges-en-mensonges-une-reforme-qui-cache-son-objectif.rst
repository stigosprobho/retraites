.. index::
   pair: Tract; CNT38 (2023-04-14)

.. _cnt38_2023_04_14:

=============================================================================================
2023-04-14 **De mensonges en mensonges, une réforme qui cache son objectif !** par CNT38
=============================================================================================

- https://ul38.cnt-f.org/2023/04/14/de-mensonges-en-mensonges-une-reforme-qui-cache-son-objectif/

:download:`Tract au format PDF <pdfs/tract_retraites_cnt38_2023_04_06.pdf>`


Introduction
==============

Nous sommes déterminé·e·s à **continuer la lutte** contre cette nouvelle attaque
de nos droits qu’est la réforme des retraites Macron-Borne.

Les buts visés par cette réforme sont de nous faire travailler plus longtemps,
de réduire et privatiser les pensions de retraite et d’augmenter la précarité.

L’allongement de l’espérance de vie serait une justification au “travailler plus”
======================================================================================

Faire le lien entre espérance de vie et retraite relève d’une logique où
l’on vivrait pour travailler.
Sauf que l’on devrait travailler seulement pour produire ce dont nous avons
besoin pour vivre, alors que dans l’organisation sociale dans laquelle
nous vivons – le capitalisme – le travail, pour l’enrichissement de
quelques uns, se traduit par la volonté de nous faire travailler toujours plus.

Ne nous laissons pas berner par ce faux lien entre espérance de vie et
retraite.

Et si nous nous arrêtons sur la question de l’allongement de l’espérance
de vie, regardons l’espérance de vie en bonne santé : 23 % des plus pauvres
sont déjà morts à 65 ans contre 5 % des plus riches et les hommes ouvriers
vivent 6 ans de moins que les cadres.

Les pauvres cotisent donc pour une retraite qu’ils et elles ont moins de
chance de toucher et moins longtemps.

**La retraite des pauvres finance celle des plus riches !**

Le “trou” de la caisse des retraites… ça creuse, ça creuse
=================================================================

L’État a crée le COR, Conseil d’orientation des retraites sous l’égide
du ou de la Première Ministre, pour analyser le financement des retraites.

Cette institution déclare que les dépenses de retraites sont stables et
ont même tendance à diminuer à long terme.

Alors que faut-il de plus pour prouver le mensonge du gouvernement à propos
d’un supposé déficit des caisses de retraite ?

Mais au-delà des faits énoncés par le COR, un trou, ça se creuse !
Et le moins qu’on puisse dire, c’est que le gouvernement creuse !
Nos salaires sont constitués de 2 parts. La part dite directe, qu’on a
ppelle communément le salaire brut ; et la part indirecte, parfois appelée
“part patronale” bien que ce soit une part de nos salaires, obtenue par
notre production.

Cette part indirecte alimente principalement les caisses de solidarité,
santé, retraite et chômage. Alors lorsque régulièrement nous entendons
Macron ou l’un de ses ministres annoncer que « pour aider les entreprises
nous allons diminuer les charges [cotisations] sociales », il faut entendre «
nous allons prendre [voler, puisque c’est sans aucune concertation] une
part des salaires pour l’offrir [puisque sans aucune contrepartie] au patronat. »

Il n’y a donc, pour les années à venir, pas de déficit des caisses de
retraites et ce malgré le pillage de ces caisses par le gouvernement.
Et cela s’explique aisément par le fait qu’une personne en 2020 produit
plus qu’en 1980, selon une augmentation bien supérieure à celle de la
population et donc des besoins.

Le système actuel permet donc, s’il n’était pas détourné, de partir plus
TÔT à la retraite. Le but étant que nous travaillons toutes, tous, mieux,
moins et selon nos capacités.

Le calcul actuel des retraites : une perpétuation des inégalités… jusqu’à la mort
======================================================================================

Si vous avez un bas salaire, vous aurez une petite retraite.

Si vous avez une carrière hachée, vous aurez une petite retraite.

Si vous avez été précaire, auto-entrepreneur·euse, flexible, agile, si
vous avez eu de multiples employeurs, vous aurez une petite retraite.

Si vous ne pouvez pas atteindre l’âge légal de départ parce que fatigué·e,
amoindri·e, cassé·e, vous aurez une petite retraite.

Et qui se retrouvent les plus exposées à ces critères ?

Les femmes !

Deux chiffres concluent toutes les analyses comparatives : les hommes ont
un salaire 28 % supérieur et une retraite 67 % supérieure à celle des
femmes !

« Évidemment, si vous reportez l’âge légal [ce que veut donc imposer la
réforme des retraites], elles sont un peu pénalisées. On n’en disconvient
absolument pas. » déclare, sans gêne, le ministre Riester.

Cette réforme est un miroir grossissant des inégalités au cours de la vie
et au travail.

Et si l’objectif, c’était plus de retraites du tout ? En route vers la privatisation finale !
==================================================================================================

Atteindre 64 ans pour faire valoir ses droits à la retraite ne sera pas
donné à tout le monde. Et si vous partez avant, vos cotisations seront
décotées, c’est-à-dire que votre retraite ne sera pas proportionnelle à
vos trimestres cotisés, mais inférieure.

Et si vous partez à 64 ans sans avoir atteint les 43 annuités, vos cotisations
seront également décotées.

On peut conclure de ce système comptable que tout est fait pour que nous
cherchions une autre solution pour espérer une retraite “normale”.

Et elle est déjà toute proposée par des entreprises privées, banques,
assurances et mutuelles : cotiser à un complément de retraite.

On passe d’une logique de retraite par et pour nous toutes et tous à une
logique individualiste : d’une retraite par répartition à une retraite
par capitalisation.

**On passe de la solidarité au chacun·e pour sa gueule**.

Et d’une retraite solidaire, organisée pour toute la population, on bascule
vers une retraite gérée par des entreprises privées qui bien sûr génèrera
des bénéfices, intérêts et dividendes pour les toujours plus riches.

Que les pauvres restent pauvres, que les riches s’enrichissent, gérer sa
vie comme une entreprise individuelle, voilà le rêve du gouvernement.


Ce qu’on veut maintenant
============================

- Gestion des caisses par les travailleuses·eurs, sans État ni patronat.
- Égalité des salaires et des pensions retraites.
- Retrait de la “réforme” et des contre-réformes depuis 1993.
- Indexation des pensions sur l’inflation.
- Aucune pension inférieure au Smic.
- Refus des retraites par capitalisation.

Nos perspectives pour un autre futur
==========================================

- Travailler tous, toutes, moins, mieux et autrement.
- Prendre les décisions collectivement à la base.
- Partager égalitairement les richesses.
- Abolir le salariat et limiter l’exploitation des ressources.

**A LUTTE CONTINUE !**
