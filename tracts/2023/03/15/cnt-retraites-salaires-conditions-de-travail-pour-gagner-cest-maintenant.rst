.. index::
   pair: CNT; Tract Retraites, salaires, conditions de travail : Pour gagner, c’est maintenant !

.. _cnt_appel_pour_gagner_2023_03_15:

============================================================================================================
2023-03-15 Tract CNT **Retraites, salaires, conditions de travail : Pour gagner, c’est maintenant !**
============================================================================================================

- https://ul38.cnt-f.org/
- https://ul38.cnt-f.org/2023/03/19/education-retraites-salaires-conditions-de-travail-pour-gagner-cest-maintenant/

:download:`Télécharger le tract au format PDF <pdfs/tract_appel_cnt_educ38_retraites.pdf>`


Retraites, salaires, conditions de travail : Pour gagner, c’est maintenant !
===============================================================================

L’ensemble de la panoplie autoritaire permise par la Ve République a été
utilisée.
Le gouvernement est allé au terme de ses dénis, de ses mépris, et de sa
violence.

Le 49.3 arrive comme la cerise pourrie sur le gâteau empoisonné, et c’est
la victoire du mouvement social qui se dessine.

Nous avions déjà gagné une première fois sur le volet argumentaire, pour
dévoiler, dénoncer et détruire leur projet, au point qu’ils ne puissent
plus en parler sans que leurs mensonges et leurs manipulations ne soient
contredites dans la minute.

Une seconde victoire a eu lieu jeudi 16 mars 2023: celle du maintien de
notre mobilisation massive et de notre détermination, qui a abouti à faire
tomber la possibilité d’une légitimation de leur réforme réactionnaire
par l’Assemblée nationale.

Le petit financier autoritaire qui avait décidé de détruire la vie des
travailleuses et des travailleurs pour permettre au grand capital de
poursuivre sa course folle, et ses débauches, est désormais coincé, isolé,
contraint à tenter un énième passage en force.

Tout le monde le dit : il est le seul et unique responsable de la crise,
et toutes les colères du pays, quelles que soient les formes de leur
expression, sont désormais légitimes.

Le délitement du gouvernement et des politiques néolibérales est amorcé.

La dernière manche pour l’emporter définitivement se joue désormais.

Vaincre la bête immonde
===========================

Mais la suite n’est pas neutre.

Notre victoire ne pourra être définitive si nous ne chassons pas dans le
même temps les fascistes qui rôdent, et entendent bien profiter de l’occasion
pour se saisir des institutions autoritaires de la Ve République.

La bourgeoisie, comme de tout temps, a déjà fait son choix, et est toute
disposée à porter l’extrême droite au pouvoir, pourvu qu’elle conserve
ses intérêts.

Le Rassemblement national, le doigt sur la couture, tente de son côté de
dissimuler son accord réel avec les politiques néolibérales mises en place
depuis 40 ans.
Il souhaite apparaître aux yeux du plus grand nombre de gens comme
l’opposant principal du gouvernement, et représenter une voie de sortie
à la crise crédible en exprimant son soutien des travailleurs et des
travailleuses.

Pourtant, aujourd’hui comme hier, il affiche son rejet farouche des
syndicats, son opposition aux grèves et aux blocages.

Des groupes fascistes s’en prennent aux cortèges, et attaquent régulièrement des
manifestations et des locaux militants.

**Les masques doivent tomber, et vite.**

Nous devons faire sombrer l’extrême-droite dans les caves de l’histoire.

**Il faut le dire et le répéter partout, l’extrême-droite est l’alliée de
la bourgeoisie, et son arrivée au pouvoir ne fera qu’empirer notre situation.**

En finir, maintenant !
==========================

Parce que nous avons affaire à des gens furieusement disposés à passer
en force, parce que si la victoire se profile, elle n’est pas acquise,
nous devons poursuivre et amplifier nos mobilisations, à l’unisson des
secteurs de l’énergie, des transports, de la voirie...

L’éducation aussi est un secteur « stratégique » !
Alors, notre devoir, c’est d’agir tout de suite, tous et toutes.

Par la grève reconductible, les blocages, les sabotages, les occupations,
les manifestations, par la grève massive, dès ce lundi durant les épreuves
du bac, le 23 mars 2023, et les jours suivant, jusqu’au retrait de cette
réforme, et au-delà.

Pour construire un monde à notre mesure, où chacune et chacun pourra
prendre part aux décisions collectives, dans l’intérêt de toutes et tous,
et celui de la planète.

Pour reprendre la main contre le capital délirant qui nous broie et préférera
toujours tout brûler plutôt que d’arrêter ses destructions.

Pour partager les richesses entre toutes et tous, parce qu’aucune vie
n’est supérieure ou plus légitime qu’une autre.

Mettons fin à leur monde violent et lacrymogène, et construisons ensemble
une société où l’égalité et la liberté aient du sens pour toutes et tous.

Les mauvais jours finiront.

En grève, toutes et tous ensemble, en AG et dans la rue, partout !

