
.. _creation_100jours:

==========================================================================================================================================================================
2023-04-22  **Création du site https://100joursdezbeul.fr/ (jour 005)** par Cedric Rossi
==========================================================================================================================================================================

- https://100joursdezbeul.fr/
- https://france.attac.org/se-mobiliser/retraites-pour-le-droit-a-une-retraite-digne-et-heureuse/article/on-ne-les-lache-pas-la-carte-des-mobilisations
- https://piaille.fr/@cedricr
- https://piaille.fr/@cedricr.rss
- https://syndicat.solidaires.org/@SolInfoNat
- https://syndicat.solidaires.org/@SolInfoNat.rss
- https://piaille.fr/@cedricr/110243802340534977

::


    100joursdezbeul AT solidairesinformatique DOT org


Création le samedi 22 avril 2023
=======================================

.. figure:: images/creation_site.png
   :align: center

   https://piaille.fr/@cedricr/110243802340534977


Puisqu'une saine compétition ne peut être que bénéfique à toustes, je suis
très heureux de vous présenter

https://100joursdezbeul.fr/

Quel département gagnera la course à l'apaisement?

L'#herault a pour l'instant une belle longueur d'avance!

Merci à tous les camarades qui ont participé à l'élaboration de ce projet ;
relectures et avis sont les bienvenus, j'apprecierais en particulier
une vérification rapide de l'accessibilité !

#foutonslezbeul #intervilles #casseroles #100jours


Merci en particulier à @Attac pour leurs données
https://france.attac.org/se-mobiliser/retraites-pour-le-droit-a-une-retraite-digne-et-heureuse/article/on-ne-les-lache-pas-la-carte-des-mobilisations !


|jo| 100 jours de zbeul **Quels sont les départements qui zbeulent le plus ?**
================================================================================================

- https://100joursdezbeul.fr/

.. figure::  images/jo_casseroles.png
   :align: center


Règles du jeu
--------------

- https://100joursdezbeul.fr/regles-du-jeu


En partant des données compilées par Attac, on assigne manuellement à
chaque événement un type de personne ciblée et un type d’action.

Les données finales sont accessibles dans le code source.
Vérifications sont les bienvenues !

Chacune de ces actions attribue un certain nombre de points au département
où elles se passent ; le type de personne ciblée permet de multiplier
ces points.

Là où plusieurs personnalités sont ciblées, les points sont comptés
pour chacune.
