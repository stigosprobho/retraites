.. index::
   pair: Formulaire; zbeul

.. _formulaire_zbeul:

==========================================================================================================================================================================
2023-04-28 **Formulaire pour proposer un événement**
==========================================================================================================================================================================

- https://syndicat.solidaires.org/@SolInfoNat/110276299423178844

Maintenant, vous pouvez soumettre vos actions en vue de l'apaisement du
pays pour #100joursdezbeul via un formulaire: https://framaforms.org/100-jours-de-zbeul-proposer-un-evenement-1682372493

Bon zbeul, et bonne #grévilla camarades !

