.. index::
   pair: 100JoursDeZbeul; proposer un événement

.. _proposer_un_evenement:

==========================================================================================================================================================================
**Proposer un événement**
==========================================================================================================================================================================

- https://framaforms.org/100-jours-de-zbeul-proposer-un-evenement-1682372493

.. figure:: images/page_formulaire.png
   :align: center

   https://framaforms.org/100-jours-de-zbeul-proposer-un-evenement-1682372493


Vous pouvez utiliser ce formulaire pour soumettre un événement ayant eu
lieu dans votre département, ou bien pour faire une réclamation sur un
événement passé -- pensez à le préciser dans le champ de commentaire.

