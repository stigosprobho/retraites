
.. _ldh_droits:

=================================================================================
LDH **Défendre vos droits**
=================================================================================

- https://www.ldh-france.org/defendre-vos-droits/


But de la LDH
==============

La Ligue des droits de l’Homme défend les droits fondamentaux énoncés
dans les instruments juridiques nationaux, européens et internationaux
relatifs aux droits de l’Homme et œuvre à leur effectivité.

Elle combat l’injustice, l’illégalité, l’arbitraire, l’intolérance, toute
forme de racisme et de discrimination, toutes les violences et toutes
les mutilations sexuelles, toutes les tortures, tous les crimes de guerre,
tous les génocides et tout crime contre l’humanité.

Elle lutte en faveur du respect des libertés individuelles en matière
de traitement des données informatisées et contre toute atteinte à la
dignité, à l’intégrité et à la liberté du genre humain.

Elle concourt au fonctionnement de la démocratie et agit en faveur de la laïcité.

