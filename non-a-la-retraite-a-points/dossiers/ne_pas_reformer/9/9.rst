
.. _dossier_ne_pas_reformer_2019_12_03_9:

================================================================================
ÉPISODE 9/10 : Les classes moyennes paient-elles pour tout le monde ?
================================================================================

.. seealso::

   - https://charliehebdo.fr/2019/12/economie/reforme-des-retraites-passion-francaise-pour-les-inegalites/


.. contents::
   :depth: 3

Introduction
==============

On l’a dit, le système de retraites est inégalitaire, en raison des différences
considérables d’espérance de vie.

Aujourd’hui, c’est Robert le maçon qui finance la retraite de Françoise, la
prof de français de ses enfants.

Mais, plus généralement, il est faux de penser qu’en France, les riches et les
classes moyennes paient pour les pauvres.

**En fait, c’est l’inverse.**
