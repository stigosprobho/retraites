.. index::
   pair: Economiste ; Jacques Littauer
   pair: Economiste ; Gilles Raveaud
   ! Gilles Raveaud
   ! Jacques Littauer


.. _gilles_raveaud:

=========================================================
Gilles Raveaud alias Jacques Littauer (Charlie Hebdo)
=========================================================

.. seealso::

   -  https://twitter.com/RaveaudGilles
   - :ref:`dossier_ne_pas_reformer_2019_12_03`
