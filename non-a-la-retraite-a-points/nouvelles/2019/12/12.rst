.. index::
   pair: 2019-12 ; Retraites

.. _luttes_retraites_2019_12:

===========================================
Décembre 2019
===========================================

.. seealso::

   - http://ici-grenoble.org/agenda/liste.php

.. toctree::
   :maxdepth: 4


   05/05
   03/03
