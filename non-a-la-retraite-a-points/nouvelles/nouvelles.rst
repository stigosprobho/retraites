.. index::
   pair: Nouvelles ; Retraites

.. _nouvellesèa_points:

===========================================
Nouvelles
===========================================

.. seealso::

   - http://ici-grenoble.org/
   - http://ici-grenoble.org/agenda/liste.php
   - https://38.demosphere.net/?a=1577836800#d1-1
   - https://paris.demosphere.net/

.. toctree::
   :maxdepth: 4

   2020/2020
   2019/2019
