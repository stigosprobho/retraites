.. index::
   pair: 2020-01-13 ;  un projet de loi dangereux par Henri Sterdyniak


.. _projet_dangereux_2020_01_13:

============================================================================================
Lundi 13 janvier 2020 Analyse Retraites : un projet de loi dangereux par Henri Sterdyniak
============================================================================================

.. seealso::

   - :ref:`dossier_projet_dangereux_2020_01_13`


.. contents::
   :depth: 3
