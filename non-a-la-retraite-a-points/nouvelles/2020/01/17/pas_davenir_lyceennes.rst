
.. _jeunesse_2020_01_17:

=======================================================================
Vendredi 17 janvier 2020 : **La jeunesse s'inquiète pour son avenir**
=======================================================================



.. figure:: pas_davenir_lyceennes.jpeg
   :align: center


Le système actuel met en péril notre futur, en détruisant un à un chacun de nos
repères.


Nous ne voulons plus vivre dans la peur.


La jeunesse se révolte contre l'Etat qui rend impossible le futur dont nous
revions.
