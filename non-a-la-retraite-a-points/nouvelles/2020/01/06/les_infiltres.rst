

.. _les_infiltres_2020_01_06:

=========================================================================================
Lundi 6 janvier 2020 : **Nous, cadres sup, aux côtés des grévistes** par Les infiltrés
=========================================================================================

.. seealso::

   - https://www.liberation.fr/debats/2020/01/06/nous-cadres-sup-aux-cotes-des-grevistes_1771808
   - https://twitter.com/Les__Infiltres
   - https://infiltres.fr/

.. contents::
   :depth: 3


Introduction
=============


Un collectif de hauts fonctionnaires et de cadres du public et du privé appelle
à rejeter la réforme des retraites.

D’autres pistes sont possibles, comme redonner la main aux salariés sur un
système qui leur échappe.



Tribune
=========


Le gouvernement ne se préoccupe des inégalités que pour dénoncer les supposés
privilèges des régimes spéciaux et dresser ainsi les Français les uns contre
les autres.

Mais la manœuvre est grossière. Tout le monde comprend que les vrais privilégiés
ne sont pas les quelques derniers détenteurs de ces régimes ; vestiges d’anciennes
luttes sociales victorieuses par ailleurs largement laminés depuis des années.

Oubliez donc les cheminots, les fonctionnaires, les profs, car les vrais
privilégié·es, c’est nous ! Passé·es par Polytechnique, Centrale, Sciences-Po
et autres grandes écoles, nous sommes maintenant hauts fonctionnaires,
cadres dirigeants du public ou du privé.

Notre position nous permet de ne pas connaître la précarité financière et de
rester relativement protégé·es de la mondialisation et des politiques d’austérité.
Et pourtant nous rejetons en bloc la politique menée par M. Macron.
