.. sidebar:: Oui à la retraite par répartition !

    :Version: |version|

.. _non_retraite_a_points:

=================================================================
**Non à la retraite à points!!**
=================================================================

- https://38.demosphere.net/?a=1577836800#d1-1
- http://ici-grenoble.org/agenda/liste.php
- :ref:`genindex`


.. figure:: contre_les_tyrans.png
   :align: center

   https://www.hacking-social.com/2019/12/03/comment-desobeir-quelques-listes/

.. toctree::
   :maxdepth: 3

   alternatives/alternatives
   dossiers/dossiers
   femmes/femmes

.. toctree::
   :maxdepth: 6

   nouvelles/nouvelles

.. toctree::
   :maxdepth: 2

   personnes/personnes
