.. index::
   pair: Occupations; MSH

.. _msh:

=================================================================================
Occupation de la MSH (Maison des sciences de l'Homme )
=================================================================================

- https://www.msh-alpes.fr/
- https://www.openstreetmap.org/#map=19/45.19188/5.77355

::

    Maison des Sciences de l'Homme-Alpes
    1221 avenue centrale - Domaine universitaire
    38400 Saint-Martin-d'Hères
    +33 (0)4 76 01 26 45


.. raw:: html

   <iframe width="800" height="400" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"
       src="https://www.openstreetmap.org/export/embed.html?bbox=5.7709738612175%2C45.19108061900396%2C5.775453150272369%2C45.19267976706065&amp;layer=mapnik"
       style="border: 1px solid black">
   </iframe><br/>
   <small><a href="https://www.openstreetmap.org/#map=19/45.19188/5.77321">Afficher une carte plus grande</a></small>



.. toctree::
   :maxdepth: 3

   2023/2023
