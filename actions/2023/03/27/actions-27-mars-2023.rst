
.. _actions_2023_03_27:

==========================================================================
**Actions du lundi 27 mars 2023**
==========================================================================

- https://www.mediamanif.org/
- https://www.ici-grenoble.org/agenda
- https://38.demosphere.net/
- :ref:`argumentaires`


#64AnsCestToujoursNon #ReformedesRetraite #Grenoble #IciGrenoble #MediaManif


**Agenda ici-grenoble**
==========================

- https://www.ici-grenoble.org/article/grevue-de-presse-les-10-infos-de-mars

.. figure:: images/ici_grenoble_2023_03_27.png
   :align: center

   https://www.ici-grenoble.org/agenda


Liens
-------

- https://www.ici-grenoble.org/evenement/journee-fac-ouverte-decouvrir-comprendre-et-mener-des-luttes
- https://www.ici-grenoble.org/evenement/table-ronde-un-decryptage-critique-de-la-reforme-des-retraites
- https://www.ici-grenoble.org/evenement/cinema-we-are-coming-chronique-dune-revolution-feministe
- https://www.ici-grenoble.org/evenement/formation-anti-repression-que-faire-en-cas-de-violences-policieres-ou-darrestation-en-manifestation
- https://www.ici-grenoble.org/evenement/rencontres-quelles-lecons-tirer-des-occupations-decoles-a-grenoble-et-des-luttes-de-personnes-sans-papiers-a-chronopost
- https://www.ici-grenoble.org/evenement/special-retraites-savez-vous-quelle-reserve-de-rage-vous-venez-de-liberer


Demosphere 38
=================

.. figure:: images/demosphere_38_2023_03_27.png
   :align: center

   https://38.demosphere.net/


