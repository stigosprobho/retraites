
.. _actions_2023_03_30:

==========================================================================
**Actions du jeudi 30 mars 2023**
==========================================================================

- https://www.mediamanif.org/
- https://www.ici-grenoble.org/agenda
- https://38.demosphere.net/
- :ref:`argumentaires`


#64AnsCestToujoursNon #ReformedesRetraite #Grenoble #MediaManif


**Agenda ici-grenoble**
==========================

- https://www.ici-grenoble.org/article/grevue-de-presse-les-10-infos-de-mars


.. figure:: images/ici_grenoble_2023_03_30.png
   :align: center

   https://www.ici-grenoble.org/agenda


Liens
-------

- https://www.ici-grenoble.org/evenement/rassemblement-de-soutien-aux-eboueurs-grevistes-blocage-des-dechets
- https://www.ici-grenoble.org/evenement/rassemblement-de-soutien-aux-grevistes-dathanor-blocage-de-lincinerateur
- https://www.ici-grenoble.org/evenement/manifestation-interprofessionnelle-et-etudiante-contre-la-reforme-des-retraites
- https://www.ici-grenoble.org/evenement/piquet-de-greve-des-salariees-du-planning-familial
- https://www.ici-grenoble.org/evenement/permanence-dautodefense-pour-lacces-aux-droits-caf-secu-titres-de-sejour-dossiers-a-remplir
- https://www.ici-grenoble.org/evenement/permanence-du-dal-isere-pour-tout-probleme-de-logement
- https://www.ici-grenoble.org/evenement/ag-interprofessionnelle-contre-la-reforme-des-retraites
- https://www.ici-grenoble.org/evenement/rencontre-debat-les-origines-coloniales-et-les-causes-neocoloniales-des-migrations-forcees
- https://www.ici-grenoble.org/evenement/assemblee-generale-de-lassociation-ancrage-autodefense-feministe
- https://www.ici-grenoble.org/evenement/concert-rencontres-roza-tournee-musicale-en-velo-solaire
- https://www.ici-grenoble.org/evenement/rassemblement-en-soutien-aux-deux-manifestants-de-sainte-soline-dans-le-coma-et-aux-200-autres-blesse-e-s
- https://www.ici-grenoble.org/evenement/assemblee-dextinction-rebellion-grenoble
- https://www.ici-grenoble.org/evenement/special-retraites-un-pays-qui-se-souleve-frederic-lordon


.. _soutien_soline_2023_03_30:

2023-03-30  🇫🇷 |solidarite| **Rassemblement en soutien aux deux manifestants de Sainte-Soline dans le coma et aux 200 autres blessé-e-s**
--------------------------------------------------------------------------------------------------------------------------------------------------

- https://www.ici-grenoble.org/evenement/rassemblement-en-soutien-aux-deux-manifestants-de-sainte-soline-dans-le-coma-et-aux-200-autres-blesse-e-s
- https://blogs.mediapart.fr/les-soulevements-de-la-terre/blog/280323/manifestants-dans-le-coma-appel-rassemblements-jeudi-30-mars-devant-les-prefecture
- https://reporterre.net/Blesses-a-Sainte-Soline-les-secours-ont-ils-ete-freines-par-les-forces-de-l-ordre
- https://lessoulevementsdelaterre.org/
- https://reporterre.net/Gerald-Darmanin-annonce-la-dissolution-des-Soulevements-de-la-Terre
- https://www.ici-grenoble.org/evenement/le-31-juillet-1977-pendant-la-manifestation-contre-superphenix-vital-michalon-est-tue-par-une-grenade-a-creys-malville-isere
- https://www.ici-grenoble.org/article/bilan-la-repression-policiere-des-gilets-jaunes

Samedi 25 mars, plus de 30 000 personnes ont manifesté contre les mégabassines
dans les Deux-Sèvres. Face aux manifestant-e-s, 3200 gendarmes et la volonté
d'écraser brutalement ce mouvement écologiste.

Bilan : Près de 200 manifestant-e-s blessé-e-s, dont 40 gravement et deux dans le coma.

Le message est clair : pour défendre la FNSEA et l'agriculture climaticide,
le gouvernement est prêt à tuer.

Ce jeudi 30 mars à 19h devant la Préfecture de l'Isère, un rassemblement
de soutien s'organise pour les deux manifestants dans le coma, les centaines
de blessé-e-s de Sainte-Soline et du mouvement des retraites.

Pour prendre un peu de recul et comprendre la situation, nous vous
recommandons plusieurs témoignages et analyses :

- Un récapitulatif des violences policières à Sainte-Soline : Sainte-Soline :
  `l’ampleur de la répression policière mise en cause <https://reporterre.net/Sainte-Soline-l-ampleur-de-la-repression-policiere-mise-en-cause>`_

- Le témoignage d'un médecin sur place : `Médecin à Sainte-Soline, je témoigne de la répression <https://reporterre.net/Medecin-a-Sainte-Soline-je-temoigne-de-la-repression>`_

- Les obstructions des forces de police envers les secours :
  Blessés à Sainte-Soline : `les secours ont-ils été freinés par les forces de police ? <https://reporterre.net/Blesses-a-Sainte-Soline-les-secours-ont-ils-ete-freines-par-les-forces-de-l-ordre>`_

- Le témoignage de deux mainifestants : `Le piège de Sainte-Soline <https://lundi.am/Le-piege-de-Sainte-Soline>`_

- La volonté de l'État de dissoudre le mouvement `Les Soulèvements de la Terre <https://lessoulevementsdelaterre.org/>`_
  `Darmanin annonce la dissolution des Soulèvements de la Terre <https://reporterre.net/Gerald-Darmanin-annonce-la-dissolution-des-Soulevements-de-la-Terre>`_

- Une analyse d'Hervé Kempf : `Après Sainte-Soline, repenser la lutte <https://reporterre.net/Apres-Sainte-Soline-repenser-la-lutte>`_

Pour prendre un peu de recul historique sur les violences d'État :

- La mort de Rémi Fraisse : `Le 25 octobre 2014, un gendarme tue Rémi Fraisse à Sivens <https://www.ici-grenoble.org/evenement/le-25-octobre-2014-un-gendarme-tue-remi-fraisse-a-sivens>`_

- La mort de Vital Michallon : `Le 31 juillet 1977, pendant la manifestation
  contre Superphénix, Vital Michalon est tué par une grenade à Creys-Malville (Isère) <https://www.ici-grenoble.org/evenement/le-31-juillet-1977-pendant-la-manifestation-contre-superphenix-vital-michalon-est-tue-par-une-grenade-a-creys-malville-isere>`_

- Le bilan de la répression des Gilets Jaunes en 2018 : `Plus de 5000 arrestations,
  plus de 1000 blessé-e-s, des dizaines de mutilations, deux coma et un décès <https://www.ici-grenoble.org/article/bilan-la-repression-policiere-des-gilets-jaunes>`_


Assemblée d'Extinction Rebellion Grenoble
-----------------------------------------------

- https://www.ici-grenoble.org/evenement/assemblee-dextinction-rebellion-grenoble
- https://www.ici-grenoble.org/structure/extinction-rebellion-grenoble
- https://extinctionrebellion.fr/branches/grenoble/


Le collectif Extinction Rebellion Grenoble, l'antenne locale du mouvement
international Extinction Rebellion, organise une assemblée pour préparer
les actions à venir.

Pour plus d'infos sur Extinction Rebellion Grenoble, c'est ici

À 19h30

Au 38 rue d'Alembert
Grenoble




Demosphere 38
=================

- https://38.demosphere.net/

.. figure:: images/demosphere38_2023_03_30.png
   :align: center

   https://38.demosphere.net/


.. figure:: ../28/images/affiche_contre_macron_et_son_monde.jpg
   :align: center
