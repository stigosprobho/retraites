.. index::
   pair: Antisémitisme; Fontaine

.. _actions_2023_03_31:

==========================================================================
**Actions du vendredi 31 mars 2023**
==========================================================================

- https://www.mediamanif.org/
- https://www.ici-grenoble.org/agenda
- https://38.demosphere.net/
- https://www.le-tamis.info/evenements
- :ref:`argumentaires`

.. figure:: images/antifa.png
   :align: center

#64AnsCestToujoursNon #ReformedesRetraite #Grenoble #MediaManif


**Agenda ici-grenoble**
==========================

- https://www.ici-grenoble.org/article/grevue-de-presse-les-10-infos-de-mars

.. figure:: images/ici_grenoble_2023_03_31.png
   :align: center

   https://www.ici-grenoble.org/agenda


Liens
-------

- https://www.ici-grenoble.org/evenement/fin-de-la-treve-hivernale-relance-des-expulsions-et-des-coupures
- https://www.ici-grenoble.org/evenement/rencontre-avec-les-auterices-de-qui-seme-le-vent-recolte-la-tapette-sur-les-groupes-de-liberation-homosexuels
- https://www.ici-grenoble.org/evenement/permanence-du-magasin-gratuit-du-38-rue-dalembert-don-de-vetements-aliments-objets
- https://www.ici-grenoble.org/evenement/rencontres-regionales-des-luttes-pour-le-droit-au-logement
- https://www.ici-grenoble.org/evenement/conference-histoire-croisee-du-chocolat-et-du-colonialisme-europeen
- https://www.ici-grenoble.org/evenement/assemblee-generale-de-lutte-contre-la-reforme-des-retraites
- https://www.ici-grenoble.org/evenement/conference-une-analyse-critique-de-la-reforme-des-retraites
- https://www.ici-grenoble.org/evenement/reunion-publique-laccaparement-des-terres-et-des-ressources-dans-la-metropole-grenobloise
- https://www.ici-grenoble.org/evenement/cine-rencontres-balad-barra-sur-les-revoltes-en-egypte
- https://www.ici-grenoble.org/evenement/documentaire-choc-sept-hivers-a-teheran-sur-les-droits-des-femmes-en-iran
- https://www.ici-grenoble.org/evenement/cinema-we-are-coming-chronique-dune-revolution-feministe


Le tamis
============

- https://www.le-tamis.info/evenements

.. figure:: images/tamis_2023_03_31.png
   :align: center

   https://www.le-tamis.info/evenements



Un rassemblement contre le fascisme et en soutien à notre camarade Édouard Schoene au 22 Avenue Jean Jaurès à Fontaine à 17h30
----------------------------------------------------------------------------------------------------------------------------------

- https://www.le-tamis.info/evenement/rassemblement-contre-le-fascismee


.. figure:: images/antifa.png
   :align: center


Le rassemblement, organisé par la section du PCF de Fontaine, aura  lieu
au droit de la plaque du souvenir de l’assassinat de dirigeants des mouvements
de résistance et du Parti communiste français, Antoine POLOTTI et Marco LIPSZYC dit Commandant Lenoir,

Graffitis nazis à Fontaine
++++++++++++++++++++++++++++++++++

Durant le week-end des inscriptions antisémites et anticommunistes visant
nommément notre camarade Édouard Schoene sont apparues à Fontaine non
seulement sur des affichettes annonçant le loto du Travailleur Alpin,
mais également sur des panneaux manuscrits jetés dans son jardin.

Les invectives haineuses accompagnées de croix gammées sont d'une violence
inouïe,  rappelant les heures les plus noires de notre histoire, celles
du nazisme : " Drancy Auschwitz le retour vite ! Juifs cocos race de m....e",
"Judéos-comunistes" "Grill chambres à gaz", "Viva El Duce Viva Mussolini",
"À bas le communisme"...


Vidéos d'Édouard Schoene
++++++++++++++++++++++++++++

- https://www.orion-hub.fr/w/wM5mqi3WomefPG7T6XBusE (partie 1)
- https://www.orion-hub.fr/w/3SJaTgr421GoV97T3j7jVr (partie 2)


Liens
-------

- https://www.le-tamis.info/evenement/manifestation-de-leau-pas-des-puces


Demosphere 38
=================

.. figure:: images/demosphere38_2023_03_31.png
   :align: center

   https://38.demosphere.net/

Rassemblement antifasciste
-------------------------------

- https://38.demosphere.net/rv/849

Durant le week-end des inscriptions antisémites et anticommunistes visant
nommément notre camarade Édouard Schoene sont apparues à Fontaine non seulement
sur des affichettes annonçant le loto du Travailleur Alpin, mais également
sur des panneaux manuscrits jetés dans son jardin.

Les invectives haineuses accompagnées de croix gammées sont d'une violence
inouïe, rappelant les heures les plus noires de notre histoire, celles
du nazisme : «Drancy Auschwitz le retour vite! Juifs cocos race de m....e»,
«Judéos-comunistes», «Grill chambres à gaz», «Viva El Duce Viva Mussolini»,
«À bas le communisme»...

Nous condamnons avec la plus grande vigueur les auteurs de ces mots d'ordre
qui osent promouvoir le retour des camps de concentration et reprennent
la propagande affichée autrefois par le régime nazi et le régime de Pétain.

La vigilance la plus vigoureuse s'impose face à ces ignominies car malheureusement,
comme l'affirmait Berthold Brecht dans sa pièce «La résistible ascension d'Arturo Ui»,
«le ventre est encore fécond, d'où a surgi la bête immonde».

.. figure:: images/antifa.png
   :align: center



.. figure:: ../28/images/affiche_contre_macron_et_son_monde.jpg
   :align: center
