
.. _cric_2023_03_18:

====================================================
2023-03-18 **AG de luttes et rdv de la semaine**
====================================================

- https://cric-grenoble.info/infos-locales/info-locales/article/ag-de-luttes-2889


Notre assemblée a pour objectif d’appuyer les luttes en cours par la promotion,
l’organisation et le soutien à des actions directes.

En effet bien que nous appelions et participions aux manifestations massives
qui ont lieu depuis plusieurs semaines, nous avons la conviction qu’elle
ne suffiront pas à faire battre le gouvernement en retraite.

Depuis le début du mouvement social nous avons donc organisé et/ou soutenu
les blocages universitaires, des blocages de transport ou d’espaces
commerciaux ou encore des opérations de péages gratuits.

Une partie de ces actions a servi à soutenir la grève générale reconductible
en reversant les bénéfices à différentes caisses de grève.

Contre la réforme des retraites, la loi "Plein emploi" ou encore les lois
Darmanin et Kasbarian, continuons de nous réunir, en tant qu’organisation
ou individus afin de mettre en échec leurs projets bourgeois, racistes et sexistes !

Vous pourrez également discuter avec certain·es d’entre nous et prendre
une boisson chaude en manif’ sous le poulpe rouge et noir de la Roulutte.

CONTACT
agdeluttesgre@canaglie.net

