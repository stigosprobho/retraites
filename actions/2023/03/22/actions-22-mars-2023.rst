
.. _actions_2023_03_22:

======================================================
**Actions du mercredi 22 mars 2023**
======================================================

- https://38.demosphere.net/
- https://www.ici-grenoble.org/agenda
- :ref:`argumentaires`


Agenda ici-grenoble
=====================

.. figure:: images/ici_grenoble_2023_03_22.png
   :align: center

   https://www.ici-grenoble.org/agenda


Liens
-------

- https://www.ici-grenoble.org/evenement/le-22-mars-1968-loccupation-de-luniversite-nanterre-point-de-depart-de-mai-68
- https://www.ici-grenoble.org/user/mes-fichiers-a-moi/agenda/5223/Tex_Casto_mai68.pdf
- https://www.ici-grenoble.org/evenement/ag-etudiante-contre-la-reforme-des-retraites-campus
- https://www.ici-grenoble.org/evenement/assemblee-generale-de-lutte-contre-la-reforme-des-retraites
- https://www.ici-grenoble.org/evenement/repetition-ouverte-de-la-chorale-de-chants-revolutionnaires-les-barricades
- http://ici-grenoble.org/infospratiques/fiche.php?id=434
- http://barricades.int.eu.org/


Demosphere 38
=================

.. figure:: images/demosphere_38_2023_03_22.png
   :align: center

   https://38.demosphere.net/



.. figure:: images/meeting_2023_03_22.jpg
   :align: center
