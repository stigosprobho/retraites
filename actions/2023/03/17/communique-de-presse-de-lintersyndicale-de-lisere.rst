.. index::
   pair: Intersyndicale; 2023-03-17

.. _intersyndicale_2023_03_17:

=======================================================================
2023-03-17 **Communiqué de presse de l’Intersyndicale de l’Isère**
=======================================================================

Jeudi 16 décembre 2023, le gouvernement a décidé d’utiliser l’article 49-3
et a déclenché une grande colère dans la population.

Ce gouvernement porte seul la responsabilité de la crise politique et
sociale actuelle.

Ce gouvernement, n’arrivant pas à arrêter ce puissant et légitime mouvement,
utilise désormais l’arme de la répression.

À Grenoble, ce jeudi 16 mars 2023, suite à la manifestation spontanée
en réaction au 49-3, deux manifestant·es ont été placé·es en garde à vue.

Aujourd’hui 17 mars, à Marseille, six camarades grévistes de l’énergie
ont été cherchés chez eux et placés en garde à vue.

L’Intersyndicale interprofessionnelle de l’Isère condamne fermement cette
répression et exige l’absence de poursuites judiciaires.

De plus, le gouvernement menace de réquisition plusieurs secteurs d’activité
en grève reconductible, ce qui est une atteinte directe à notre droit
constitutionnel de grève.

Nous n’avons pas peur car nous sommes solidaires et déterminé·es pour
obtenir le retrait de cette réforme contestée par tous.

Nous appelons à l’amplification de la mobilisation, par la grève reconductible.

L’intersyndicale interprofessionnelle des Unions Départementales de l’Isère
Le 17 mars 2023, Grenoble

:download:`Télécharger le communiqué au format PDF <pdfs/communique-intersyndicale-isere.pdf>`
