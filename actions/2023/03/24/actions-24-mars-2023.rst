
.. _actions_2023_03_24:

==========================================================================
**Actions du vendredi 24 mars 2023**
==========================================================================

- https://www.mediamanif.org/
- https://www.ici-grenoble.org/agenda
- https://38.demosphere.net/
- :ref:`argumentaires`


#64AnsCestToujoursNon #ReformedesRetraite #Grenoble #MediaManif


**Agenda ici-grenoble**
==========================

- https://www.ici-grenoble.org/article/grevue-de-presse-les-10-infos-de-mars

.. figure:: images/ici_grenoble_2023_03_24.png
   :align: center

   https://www.ici-grenoble.org/agenda


Liens
-------

- https://www.ici-grenoble.org/evenement/atelier-comment-rendre-nos-espaces-de-vie-et-dengagement-plus-safe
- https://www.ici-grenoble.org/evenement/cine-rencontres-sur-la-planche-sur-des-femmes-en-lutte-pour-leur-survie-a-tanger
- https://www.ici-grenoble.org/evenement/conference-pour-une-ecologie-pirate-comment-ancrer-lecologie-dans-les-quartiers-populaires
- https://www.ici-grenoble.org/evenement/special-retraites-savez-vous-quelle-reserve-de-rage-vous-venez-de-liberer
- https://www.ici-grenoble.org/evenement/campus-faites-de-la-bidouille-bricolage-partage-de-savoirs-musique


Demosphere 38
=================

.. figure:: images/demosphere_38_2023_03_24.png
   :align: center

   https://38.demosphere.net/


