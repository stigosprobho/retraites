
.. _actions_2023_06_10:

=========================================================================================================================================
**Actions du samedi 10 juin 2023 #Grenoble #64AnsCestToujoursNon #ReformedesRetraites  #SeuleLaLuttePaie**
=========================================================================================================================================

- https://www.ici-grenoble.org/agenda
- https://38.demosphere.net/
- :ref:`argumentaires`


#Grenoble #64AnsCestToujoursNon #ReformedesRetraites  #SeuleLaLuttePaie


**Agenda ici-grenoble**
==========================

.. figure:: images/ici_grenoble_2023_06_10.png
   :align: center

   https://www.ici-grenoble.org/agenda

- https://www.ici-grenoble.org/evenement/le-10-juin-1961-la-naissance-du-planning-familial-de-grenoble

Demosphere 38
=================

.. figure: images/demo_2023_06_01.png
   :align: center

   https://38.demosphere.net/
