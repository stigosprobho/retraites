
.. _actions_2023_06_05:

=========================================================================================================================================
**Actions du lundi 5 juin 2023 #Grenoble #64AnsCestToujoursNon #ReformedesRetraites  #SeuleLaLuttePaie**
=========================================================================================================================================

- https://www.ici-grenoble.org/agenda
- https://38.demosphere.net/
- :ref:`argumentaires`


#Grenoble #64AnsCestToujoursNon #ReformedesRetraites  #SeuleLaLuttePaie


**Agenda ici-grenoble**
==========================

.. figure: images/ici_grenoble_2023_06_01.png
   :align: center

   https://www.ici-grenoble.org/agenda


Demosphere 38
=================

.. figure: images/demo_2023_06_01.png
   :align: center

   https://38.demosphere.net/

