
.. _actions_2023_06_01:

=========================================================================================================================================
**Actions du jeudi 1er juin 2023 #Grenoble #64AnsCestToujoursNon #ReformedesRetraites  #SeuleLaLuttePaie**
=========================================================================================================================================

- https://www.ici-grenoble.org/agenda
- https://38.demosphere.net/
- :ref:`argumentaires`


#Grenoble #64AnsCestToujoursNon #ReformedesRetraites  #SeuleLaLuttePaie


**Agenda ici-grenoble**
==========================

.. figure: images/ici_grenoble_2023_06_01.png
   :align: center

   https://www.ici-grenoble.org/agenda


Demosphere 38
=================

.. figure: images/demo_2023_06_01.png
   :align: center

   https://38.demosphere.net/




#logiciel #informatique #numérique #grenoble #internet #surveillance #cinéma #documentaire
===========================================================================================

- https://imaginair.es/@thierrybayoud/110310603337202496

Le 1er juin prochain à Grenoble sera projeté notre documentaire intitulé
« LoL - Logiciel Libre, une affaire sérieuse ».

La séance est prévue à 19h à la Maison des écologistes et sera suivie
d'un débat avec un représentant de l' @aprilorg , Jean-Christophe Becquet,
et un autre de la ville d'Échirolles, @nicolasvivant

L'entrée est libre et gratuite.

Plus de détails en suivant ce lienternet : https://metro-grenoble.eelv.fr/lol-logiciel-libre-une-affaire-serieuse/

Un grand merci @Gallorum pour l'organisation de cet événement, lequel
permet à notre travail de continuer à vivre.

#logiciel #informatique #numérique #grenoble #internet #surveillance #cinéma #documentaire

