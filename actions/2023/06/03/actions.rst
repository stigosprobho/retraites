
.. _actions_2023_06_03:

=========================================================================================================================================
**Actions du samedi 3 juin 2023 #Grenoble #64AnsCestToujoursNon #ReformedesRetraites  #SeuleLaLuttePaie**
=========================================================================================================================================

- https://www.ici-grenoble.org/agenda
- https://38.demosphere.net/
- :ref:`argumentaires`


#Grenoble #64AnsCestToujoursNon #ReformedesRetraites  #SeuleLaLuttePaie

**Agenda ici-grenoble**
==========================

.. figure:: images/ici_grenoble_2023_06_03.png
   :align: center

   https://www.ici-grenoble.org/agenda

- https://www.ici-grenoble.org/evenement/le-3-juin-1970-les-incroyables-emeutes-du-campus-de-saint-martin-d-heres
- https://www.ici-grenoble.org/evenement/atelier-clown-e-s-en-mixite-choisie-1
- https://www.ici-grenoble.org/evenement/pop-up-store-lorella-bijoux (erreur 500)

Demosphere 38
=================

.. figure: images/demo_2023_06_01.png
   :align: center

   https://38.demosphere.net/
