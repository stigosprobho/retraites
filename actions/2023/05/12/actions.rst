
.. _actions_2023_05_12:

==============================================================================================================================================================================
**Actions du vendredi 12 mai 2023 #Grenoble #64AnsCestToujoursNon #ReformedesRetraites #SeuleLaLuttePaie #100JoursDeZbeul #IntervillesDuZbeul #ExtinctionRebellionRecrute**
==============================================================================================================================================================================

- https://www.ici-grenoble.org/agenda
- https://38.demosphere.net/
- :ref:`argumentaires`


#Grenoble #64AnsCestToujoursNon #ReformedesRetraites #SeuleLaLuttePaie #100JoursDeZbeul #IntervillesDuZbeul #ExtinctionRebellionRecrute



.. _veran_2023_05_12:

|ici_grenoble| A la une **#100JoursDeZbeul #IntervillesDuZbeul Olivier Véran à Grenoble** |casserolade|
============================================================================================================

- https://www.ici-grenoble.org/evenement/casserolade-pour-la-venue-dolivier-veran-a-grenoble-pour-la-defense-des-hopitaux-publics
- https://38.demosphere.net/rv/1172 (ACCUEIL DE VÉRAN Vendredi 12 Mai  2023, de 14h à 20h)
- https://100joursdezbeul.fr/
- https://france.attac.org/se-mobiliser/retraites-pour-le-droit-a-une-retraite-digne-et-heureuse/article/on-ne-les-lache-pas-la-carte-des-mobilisations


.. figure:: images/page_une_ici_grenoble_2023_05_12.png
   :align: center

   https://www.ici-grenoble.org/article/casseroladons-olivier-veran


**Olivier Véran à Grenoble**
--------------------------------------------------------------------

Ce vendredi 12 mai, la venue d'Olivier Véran est annoncée à Grenoble.

L'intersyndicale appelle à lui faire un accueil sonore à 18h place Verdun,
devant la Préfecture de l'Isère.

Le porte-parole du gouvernement redoutant la casserolade, l'horaire et
le lieu du rassemblement peuvent changer (consultez l'agenda d'ici Grenoble).

Voici l'appel intersyndical à rassemblement :

"Ce vendredi, Olivier Véran décerne une médaille à un médecin du CHU pour
"bonne gestion de la crise Covid". Nous connaissons toutes et tous les
conditions de travail et d’accueil pour le CHU…

L'intersyndicale Isere appelle à un rassemblement pour défendre notre
système de santé. N’hésitez pas à venir avec votre matériel sonore !"


La venue d'Olivier Véran intervient en pleine crise du CHU Grenoble-Alpes :

C'est un cauchemar que nous redoutons tou-te-s : amener un-e proche aux
urgences, attendre des heures face à des équipes débordées, subir les
maltraitances involontaires de soignant-e-s épuisé-e-s, souffrir le martyr,
frôler la mort.

Ces quatre derniers mois, au moins trois personnes sont mortes aux urgences
du CHU Grenoble-Alpes, dans le désarroi et la solitude, alors que leur
pronostic vital n’était pas engagé au moment de leur admission.

Comment dans la plupart des hôpitaux de France, la crise des urgences du
CHU ne fait que s'approfondir.
Des équipes soignantes de moins en moins nombreuses doivent accueillir
de plus en plus de monde.

Au CHU Grenoble-Alpes, une cinquantaine de médecins urgentistes seraient
nécessaires, alors qu'ils dépassent rarement la trentaine.

De son côté, la fréquentation ne fait qu'augmenter, avec désormais 140
passages en moyenne par jour, 50% de plus qu'il y a dix ans.

Pour mieux saisir l'ampleur du désastre, nous vous recommandons les
témoignages d'urgentistes du CHU : Les urgences de Grenoble au bord du craquage.

Cette crise structurelle touche de nombreux autres secteurs hospitaliers,
dont la pédiatrie et la psychiatrie.

Faute de moyens et de soignant-e-s, les opérations sont déprogrammées,
les fermetures de lits se multiplient.

Comment en est-on arrivé là ? Quelles seraient les solutions ? Pour une
analyse claire et approfondie, nous vous recommandons l'excellent livre
La casse du siècle, à propos des réformes de l'hôpital public.

(Dauphiné Libéré, 20/04/23, 23/04/23, 26/04/23)


.. figure:: images/la_casse_du_siecle.png
   :align: center

Dessins/images de Véran
----------------------------

- :ref:`media_2023:olivier_veran`

**Agenda ici-grenoble**
==========================

.. figure:: images/ici_grenoble_2023_05_12.png
   :align: center

   https://www.ici-grenoble.org/agenda


- 7h00 https://www.ici-grenoble.org/evenement/fridays-for-future-grenoble-appel-a-la-greve-mondiale-pour-le-climat
- 14h00 https://www.ici-grenoble.org/evenement/rassemblement-de-fridays-for-future-grenoble
- 17h30 https://www.ici-grenoble.org/evenement/permanence-du-collectif-tangentes-metiers-du-batiment-en-mixite-choisie
- 18h00 https://www.ici-grenoble.org/evenement/dans-quelle-paix-pensez-vous-vivre-atelier-performance-artistique
- 18h00 https://www.ici-grenoble.org/evenement/fete-des-1-an-du-coup-de-pousse
- 18h00 https://www.ici-grenoble.org/evenement/casserolade-pour-la-venue-dolivier-veran-a-grenoble-pour-la-defense-des-hopitaux-publics
- 18h00 https://www.ici-grenoble.org/evenement/rencontre-avec-lautrice-de-les-femmes-musulmanes-ne-sont-elles-pas-des-femmes
- 18h30 🎥 https://www.ici-grenoble.org/evenement/cine-discussion-ni-dieu-ni-maitre-documentaire-passionnant-sur-lhistoire-de-lanarchisme
- 18h30 https://www.ici-grenoble.org/evenement/soiree-portes-ouvertes-pour-que-vive-la-bibliotheque-de-lecologie-sauvons-la-mediatheque-de-la-mnei
- 19h00 https://www.ici-grenoble.org/evenement/rencontres-et-apero-autour-du-livre-et-souvre-enfin-la-maison-close-sur-un-squat-fertile-du-20eme-siecle
- https://www.ici-grenoble.org/evenement/lipsync-party-lipsync-partout-shows-et-boom-en-soutien-a-kontrosol-en-mixite-choisie

Demosphere 38
=================

.. figure:: images/demo_2023_05_12.png
   :align: center

   https://38.demosphere.net/


- https://38.demosphere.net/rv/1172

Véran sera chez nous, un rassemblement est déclaré devant la préfecture
entre 14h et 20h.

Il doit remettre la légion d'honneur pour bon une gestion du COVID
( eh oui ! <tout est possible en macronie).

Il est possible qu'un 2nd rdv émerge entre temps car ce rendez-vous est
Top-secret (Comme le défilé en ultra-solitaire de Micron sur les champs Elysée).

Préparez vos vélos, baskets et ... casseroles ...


.. figure:: images/vomi_emeotophobie.png
   :align: center

Dessins/images de Véran
----------------------------

- :ref:`media_2023:olivier_veran`


|rebellion| Extinction rebellion recrute
============================================

- https://mobilizon.extinctionrebellion.fr/events/2d662068-0c78-4981-89eb-25c68458786a
- :ref:`grenoble_infos:extinction_rebellion`

Tu souhaites découvrir le mouvement et savoir comment t’engager ?
Nous organisons une réunion d’accueil des nouveaux/nouvelles en présentiel
au 38 à Grenoble le vendredi 12 mai à 19h.

N’hésite pas à t’inscrire à notre newsletter d’ici là : https://listes.extinctionrebellion.fr/subscription/YDieIERkgN

Pour en savoir plus sur Extinction Rebellion : https://extinctionrebellion.fr/rejoignez-nous/

En cas de questions, écris nous à grenoble@extinctionrebellion.fr



