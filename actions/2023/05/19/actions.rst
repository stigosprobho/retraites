
.. _actions_2023_05_19:

=========================================================================================================================================
**Actions du vendredi 19 mai 2023 #Grenoble #64AnsCestToujoursNon #ReformedesRetraites  #SeuleLaLuttePaie**
=========================================================================================================================================

- https://www.ici-grenoble.org/agenda
- https://38.demosphere.net/
- :ref:`argumentaires`


#Grenoble #64AnsCestToujoursNon #ReformedesRetraites  #SeuleLaLuttePaie


**Agenda ici-grenoble**
==========================

.. figure: images/ici_grenoble_2023_05_01.png
   :align: center

   https://www.ici-grenoble.org/agenda


Demosphere 38
=================

.. figure: images/demo_2023_05_01.png
   :align: center

   https://38.demosphere.net/



Glières vendredi soir 19 mai 2023 🎥
========================================

- https://citoyens-resistants.fr/spip.php?article659

A noter, les interventions du dimanche seront signées (LSF).
Une conférence, à l’étude, sera elle aussi signée

Nouveauté : Cinéma dès le vendredi à 17 h, avec un film jeune public (voir ci dessous)

Théâtre du vendredi 19 mai au soir : (Salle Tom Morel .Thorens-Glières)

- 20h00 : "Le monde commence aujourd’hui", compagnie La Cahute.

Lucas est un jeune homme d’une vingtaine d’années.

Il découvre par hasard les textes de Jacques Lusseyran, un écrivain résistant
aveugle qui a connu l’horreur de la déportation.

Cette découverte l’amène à remettre en question la manière dont il est
en train de vivre sa vie.

Il se rend compte que son existence est en train de lui échapper, qu’il
file vers une vie toute tracée qu’il n’a jamais véritablement choisie.

