
.. _actions_2023_05_24:

=========================================================================================================================================
**Actions du jeudi 24 mai 2023 #Grenoble #64AnsCestToujoursNon #ReformedesRetraites  #SeuleLaLuttePaie**
=========================================================================================================================================

- https://www.ici-grenoble.org/agenda
- https://38.demosphere.net/
- :ref:`argumentaires`


#Grenoble #64AnsCestToujoursNon #ReformedesRetraites  #SeuleLaLuttePaie


**Agenda ici-grenoble**
==========================

.. figure:: images/ici_grenoble_2023_05_24.png
   :align: center

   https://www.ici-grenoble.org/agenda


- https://www.ici-grenoble.org/evenement/le-24-mai-1864-le-droit-de-faire-greve-en-france
- |important| https://www.ici-grenoble.org/evenement/conference-de-la-quadrature-du-net-la-technopolice-et-le-controle-social-a-la-caf-et-au-pole-emploi

Demosphere 38
=================

.. figure: images/demo_2023_05_01.png
   :align: center

   https://38.demosphere.net/



|rebellion| Une initiation pour être Street médic à organisée par XR Grenoble et Alternatiba Grenoble
===========================================================================================================


Une initiation pour être Street médic à organisée par XR Grenoble et
Alternatiba Grenoble aura lieu le mercredi 24 mai au Bocal d'alternatiba,
le long de l'esplanade à Grenoble.

Vient découvrir et discuter des bases du soin en action avec une petite
partie de bases puis un temps d’échanges et de réponses aux questions.

Attention, cette formation n’est pas une formation aux premiers secours
mais bien à des conduites à tenir et astuces en cas de pépins sur les actions.

Merci de remplir ce formulaire pour vous y inscrire :

👉 grenoble.alternatiba.eu/printempsdesformations/ 👈

Le lieu et les horaires précis de la formation vous seront reconfirmés par email.

Pour être au courant des prochains événements  XR Grenoble : https://listes.extinctionrebellion.fr/subscription/YDieIERkgN

Être au courant des prochains événements Alternatiba Grenoble : https://grenoble.alternatiba.eu/

Pour en savoir plus sur Extinction Rebellion, nous rejoindre : https://extinctionrebellion.fr/rejoignez-nous/

🙋Des questions ? écris nous à grenoble@extinctionrebellion.fr
