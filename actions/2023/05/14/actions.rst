
.. _actions_2023_05_14:

=========================================================================================================================================
**Actions du dimanche 14 mai 2023 #Grenoble #64AnsCestToujoursNon #ReformedesRetraites #SeuleLaLuttePaie #BOCAL**
=========================================================================================================================================

- https://www.ici-grenoble.org/agenda
- https://38.demosphere.net/
- :ref:`argumentaires`


#Grenoble #64AnsCestToujoursNon #ReformedesRetraites #SeuleLaLuttePaie #BOCAL


**Agenda ici-grenoble**
==========================

.. figure: images/ici_grenoble_2023_05_01.png
   :align: center

   https://www.ici-grenoble.org/agenda


Demosphere 38
=================

.. figure: images/demo_2023_05_01.png
   :align: center

   https://38.demosphere.net/



Dimanche 14 mai 2023 au 8 boulevard de l'Esplanade, Alternatiba Grenoble ouvre un nouveau lieu de luttes : la Base d'Organisation Collective des ALternatives (BOCAL)
======================================================================================================================================================================

- https://www.ici-grenoble.org/evenement/inauguration-du-bocal-de-grenoble-base-dorganisation-collective-des-alternatives

Dimanche 14 mai au 8 boulevard de l'Esplanade, Alternatiba Grenoble ouvre
un nouveau lieu de luttes : la Base d'Organisation Collective des
ALternatives (BOCAL).

Il pourra accueillir des événements de plus grande envergure que la Base
d'Action Sociale et Ecologique (BASE), le bar associatif lancé par
Alternatiba en 2022, rue du Dauphiné.

Au programme de l'inauguration : concerts, ateliers, jeux... Tous les détails sont `ici <https://www.ici-grenoble.org/evenement/inauguration-du-bocal-de-grenoble-base-dorganisation-collective-des-alternatives>`_

