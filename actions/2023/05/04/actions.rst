
.. index::
   pair: Drone ; 2023-05-04

.. 🎥


.. _actions_2023_05_04:

=========================================================================================================================================
**Actions du jeudi 4 mai 2023 #Grenoble #64AnsCestToujoursNon #ReformedesRetraites #SeuleLaLuttePaie**
=========================================================================================================================================

- https://www.ici-grenoble.org/agenda
- https://38.demosphere.net/
- :ref:`argumentaires`


#Grenoble #64AnsCestToujoursNon #ReformedesRetraites  #SeuleLaLuttePaie


**Agenda ici-grenoble**
==========================

.. figure:: images/ici_grenoble_2023_05_04.png
   :align: center

   https://www.ici-grenoble.org/agenda

- 12h00 https://www.ici-grenoble.org/evenement/repas-sur-place-ou-a-emporter-proposes-par-cuisine-sans-frontieres
- |important| 15h30 https://www.ici-grenoble.org/evenement/manifestation-interprofessionnelle-contre-la-reforme-des-retraites-devant-le-medef
- 16h00 🎥 https://www.ici-grenoble.org/evenement/cinema-letabli-sur-lentrisme-revolutionnaire-dans-les-usines
- 17h00 https://www.ici-grenoble.org/evenement/permanence-du-dal-isere-pour-tout-probleme-de-logement
- |important| 18h00 https://www.ici-grenoble.org/evenement/nouvelle-reunion-gnoup-pour-la-creation-dun-centre-social-autogere-a-grenoble
- |important| |alternatiba| 18h30 https://www.ici-grenoble.org/evenement/aperotiba-soiree-de-presentation-dalternatiba-grenoble
- 18h30 https://www.ici-grenoble.org/evenement/cercle-de-lectures-feministes-de-grenoble
- 19h00 |car38|  https://www.ici-grenoble.org/evenement/atelier-deducation-popuplaire-defendre-ses-droits-tout-un-travail-sur-reservation

      - https://www.ici-grenoble.org/article/15aine-anti-repression-le-programme

- 20h50 https://www.ici-grenoble.org/evenement/cinema-sur-ladamant-sur-la-psychiatrie-alternative
- 20h55 https://www.ici-grenoble.org/evenement/cinema-je-verrai-toujours-vos-visages-sur-les-experiences-de-justice-reparative


Demosphere 38
=================

.. figure:: images/demo_2023_05_04.png
   :align: center

   https://38.demosphere.net/


- |important| |important| 15h30 https://38.demosphere.net/rv/1152 (Manifestation puis rassemblement contre le MEDEF et la réforme des retraites ! , 66 Boulevard Maréchal Foch)
- |important| |car38| https://38.demosphere.net/rv/1048 (DEFENDRE SES DROITS, TOUT UN TRAVAIL ! )

      - https://www.ici-grenoble.org/article/15aine-anti-repression-le-programme


.. _repression_2023_05_04:

2023-05-04 Répression d'Etat dans un gouvernement illibéral (pré-fasciste)
=============================================================================

- https://www.isere.gouv.fr/contenu/recherche/(searchtext)/drone/(change)/1864377964?SearchText=drone
- https://www.isere.gouv.fr/contenu/telechargement/67799/542527/file/recueil-38-2023-073-recueil-des-actes-administratifs-special.pdf
- https://www.openstreetmap.org/#map=19/45.18067/5.71733

ARRÊTÉ 38-2023- autorisant la captation, l’enregistrement et la transmission
d’images au moyen de caméras installées sur les aéronefs


:download:`ARRÊTÉ 38-2023- autorisant la captation, l’enregistrement et la transmission d’images au moyen de caméras installées sur les aéronefs <pdfs/drone_2023_05_04.pdf>`

Considérant l’annonce d’une manifestation visant les locaux du MEDEF d’une part et de
l’école de management d’autre part, sites symboliques pour les mouvements de contestation
sociale,

Considérant que les black blocks se constituent discrètement mais rapidement au sein de
cortège doivent pouvoir être identifiés très rapidement, que cette identification n’est
possible que par une prise de vue en hauteur très mobile ;

Considérant que, compte tenu du risque sérieux de troubles à l’ordre public durant la journée
du 4 mai 2023 en raison des actions annoncées, de l’ampleur de la zone visée à sécuriser, de
la mobilité des manifestants notamment des groupes à risque, de l’intérêt de disposer d’une
vision en grand angle pour permettre le maintien et le rétablissement de l’ordre public tout
en limitant l’engagement des forces au sol, le recours aux dispositifs de captation installés sur
des aéronefs est nécessaire et adapté ; qu’il n’existe pas de dispositif moins intrusif
permettant de parvenir aux mêmes fins ;

Considérant que la demande porte sur l’engagement d’une caméra aéroportée pendant la
seule durée de la menace, c’est-à-dire de 15h à 21h ; que les lieux surveillés sont strictement
limités au site susceptible de faire de dégradations, où sont susceptibles de se commettre les
atteintes que l’usage des caméras aéroportées vise à prévenir ; que la durée de l’autorisation
est également strictement limitée à la durée de la menace ; qu’au regard des circonstances
sus mentionnées, la demande n’apparaît pas disproportionnée ;
Considérant le recours à la captation, l’enregistrement et la transmission d’images fera l’objet
d’une information par plusieurs moyens adaptés ; qu’outre la publication du présent arrêté
au recueil des actes administratifs ; que de même, une information spécifique sera apportée
sur les lieux du site visé où les caméras aéroportées seront utilisées, visant à avertir les
personnes présentes qu’elles sont susceptibles d’être filmées par l’intermédiaire de tweets de
la DDSP et par voie d’affichage de l’arrêté sur le site internet de la préfecture ;
Sur proposition du directeur de cabinet du préfet ;

Arrête

Article 1er La captation, l’enregistrement et la transmission d’images par le groupement de
gendarmerie de l’Isère, est autorisée au titre de la prévention des atteintes à la sécurité des
personnes et des biens sur le secteur délimité par par la rue Esclangon, le cours Berriat, le
cours Jean Jaurès, la voie de Corato situés à Grenoble dans le cadre d’actions de mouvements
de type Black blocks, et l’appui des personnels au sol, en vue de leur permettre de maintenir
ou de rétablir l'ordre public.

Article 2 – Le nombre maximal de caméras pouvant procéder simultanément aux traitements
mentionnés à l’article 1er est fixé à une, sur le matériel suivant : MAVIC 2 ENTREPRISE ZOOM

Article 3 – La présente autorisation est limitée au périmètre géographique figurant sur le plan
joint en annexe.
Article 4 – La présente autorisation est délivrée pour la durée de la menace, soit de 15h à 21h.

Article 5 – L’information du public est assurée comme suit : **tweet et site internet de la
préfecture** ???

.. warning:: Article 5 : Information du public non vue sur twitter (https://nitter.fdn.fr/Prefet38#m)
   et bien cachée sur le site de la préfecture (il faut faire une rechetrche sur le mot 'drone',
   https://www.isere.gouv.fr/contenu/recherche/(searchtext)/drone/(change)/1864377964?SearchText=drone)



- https://www.openstreetmap.org/#map=19/45.18067/5.71733, 66 Boulevard Maréchal Foch 38000 grenoble

.. raw:: html

   <iframe width="1000" height="400" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"
      src="https://www.openstreetmap.org/export/embed.html?bbox=5.715562105178834%2C45.179896668351695%2C5.719097256660462%2C45.181443194053294&amp;layer=mapnik" style="border: 1px solid black">
   </iframe><br/>
   <small><a href="https://www.openstreetmap.org/#map=19/45.18067/5.71733">Afficher une carte plus grande</a></small>
