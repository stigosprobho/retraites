
.. _actions_2023_05_16:

=========================================================================================================================================
**Actions du mardi 16 mai 2023 #Grenoble #64AnsCestToujoursNon #ReformedesRetraites  #SeuleLaLuttePaie**
=========================================================================================================================================

- https://www.ici-grenoble.org/agenda
- https://38.demosphere.net/
- :ref:`argumentaires`


#Grenoble #64AnsCestToujoursNon #ReformedesRetraites  #SeuleLaLuttePaie


**Agenda ici-grenoble**
==========================

.. figure: images/ici_grenoble_2023_05_01.png
   :align: center

   https://www.ici-grenoble.org/agenda


Demosphere 38
=================

.. figure: images/demo_2023_05_01.png
   :align: center

   https://38.demosphere.net/



|rebellion| Une initiation au clown activisme organisée par les Nanos (collectif de clowns à grenoble)
===========================================================================================================

Une initiation au clown activisme organisée par les Nanos (collectif de
clowns à grenoble), XR Grenoble et Alternatiba Grenoble aura lieu le
mardi 16 mai à 18h à Grenoble.

Vient découvrir et t’initier au clown activisme !

Les Nanos te proposent de découvrir ce qu’est le clownactivisme en théorie,
puis en pratique via des exercices classiques, comme des ateliers
d’improvisation ou du travail sur les émotions.

Merci de remplir ce formulaire pour vous y inscrire :

👉 grenoble.alternatiba.eu/printempsdesformations/ 👈

Attention, cette formation est limitée à une jauge de 12 personnes,
n'oublie pas de t'inscrire !

Le lieu et les horaires précis de la formation vous seront communiqués par email.

Pour être au courant des prochains événements  XR Grenoble : https://listes.extinctionrebellion.fr/subscription/YDieIERkgN

Être au courant des prochains événements Alternatiba Grenoble : https://grenoble.alternatiba.eu/

Pour en savoir plus sur Extinction Rebellion, nous rejoindre : https://extinctionrebellion.fr/rejoignez-nous/

🙋Des questions ? écris nous à grenoble@extinctionrebellion.fr

