
.. _actions_2023_04_17:

=========================================================================================================================================
**Actions du lundi 17 avril 2023 Macron parle ? La rue aussi !**
=========================================================================================================================================

- https://www.ici-grenoble.org/article/macron-parle-la-rue-aussi

- https://www.ici-grenoble.org/agenda
- https://38.demosphere.net/
- :ref:`argumentaires`


#64AnsCestToujoursNon #ReformedesRetraite #Grenoble


**Agenda ici-grenoble**
==========================

.. figure:: images/ici_grenoble_2023_04_17.png
   :align: center

   https://www.ici-grenoble.org/agenda


- https://www.ici-grenoble.org/evenement/barbecue-vegetarien-pour-echanger-sur-la-suite-des-mobilisations-sur-le-campus
- https://www.ici-grenoble.org/evenement/permanence-du-garage-associatif-les-soupapes-reparer-sa-voiture
- https://www.ici-grenoble.org/evenement/documentaire-choc-sept-hivers-a-teheran-sur-les-droits-des-femmes-en-iran
- https://www.ici-grenoble.org/evenement/atelier-de-clown-activisme
- https://www.ici-grenoble.org/evenement/formation-devenir-mediateur-ice-en-action-de-desobeissance-civile
- https://www.ici-grenoble.org/evenement/resto-malap-a-prix-libre-repas-congolais-vegan-cuisine-par-des-personnes-sans-papiers
- https://www.ici-grenoble.org/evenement/projection-film-la-sociale-pour-les-caisses-de-greve
- https://www.ici-grenoble.org/evenement/macron-parle-rassemblement-contre-la-reforme-des-retraites
- https://www.ici-grenoble.org/evenement/cinema-letabli-sur-lentrisme-revolutionnaire-dans-les-usines
- https://www.ici-grenoble.org/evenement/cinema-je-verrai-toujours-vos-visages-sur-les-experiences-de-justice-reparative

Macron parle ? La rue aussi !
---------------------------------

- https://www.ici-grenoble.org/article/macron-parle-la-rue-aussi
- https://www.ici-grenoble.org/evenement/macron-parle-rassemblement-contre-la-reforme-des-retraites


Ce lundi soir, Macron a des choses à nous dire ? Nous aussi !

Ce 17 avril, un rassemblement contre la réforme des retraites s'organise
à 20h devant la mairie de Grenoble, comme dans de nombreuses mairies en France.

Rappelons que malgré le 49.3, la répression des CRS et les multiples
contre-feux médiatiques, la lutte contre la réforme des retraites continue :
l'intersyndicale appelle à faire du premier mai une journée historique
de lutte en France.

Une déferlante populaire pour marquer notre détermination et notre ténacité.

Pour échanger sur la situation politique actuelle et préparer des actions,
une nouvelle Assemblée Générale de lutte se prépare mardi 18 avril à 18h30
au 38 rue d'Alembert.

Dans ce contexte politique si sombre, face à un capitalisme de plus en
plus autoritaire, mensonger et répressif, face à l'extrême-droite en
embuscade, merci à celles et ceux qui résistent, qui manifestent, qui
s'organisent, qui bloquent, qui prennent des risques.

Rappelons qu'au plus fort de la lutte ces dernières semaines, les manifestations
ont rassemblé plus de 50 000 personnes à Grenoble selon les syndicats
(plus de 30 000 selon la police).

Les luttes se sont également durcies : énormes manifs sauvages, jet de
cocktails molotov sur la Préfecture de l'Isère, barrages filtrants aux
portes de Grenoble, opérations péage gratuit à Voreppe, sabotages...

Nous étions manifestement dans une situation pré-révolutionnaire.

Face à l'ampleur de la colère contre une réforme des retraites injuste,
les luttes vont-elles encore se durcir, et la répression policière aussi ?

Combien de temps les CRS vont-ils soutenir ce gouvernement impopulaire
au sein de leurs propres familles ?

Pour un agenda précis des assemblées générales par secteur d'activité,
des opérations de tractages ou de blocages, vous pouvez consulter
`Démosphère Isère <https://38.demosphere.net/>`_


Demosphere 38
=================

.. figure:: images/demo_2023_04_17.png
   :align: center

   https://38.demosphere.net/


librinfo74
==============

- https://librinfo74.fr/ce-lundi-17-avril-coupez-la-lumiere-de-20h-a-20h30-pendant-lallocution-de-macron/

Dans le cadre d’une action avec des collègues energéticiens, ATTAC propose
que chaque foyer coupe son arrivée électrique ou à minima se cantonne au
strict minimum en terme de consommation électrique CE LUNDI 17 AVRIL
de 20h à 20h30.

Cette action aura pour effet de créer une forte variation non contrôlée
sur le réseau électrique avec un impact européen.

« Nous vous épargnons les explications techniques mais si nous sommes
entre 3 et 6 millions, toute l’Europe sera impactée au moment de la prise
de parole de Macron.  Mettons en lumière sa mégalomanie autocrate. »

