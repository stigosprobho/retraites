
.. _actions_2023_04_28:

=========================================================================================================================================
**Actions du vendredi 28 avril 2023 #Grenoble #64AnsCestToujoursNon #ReformedesRetraites #AG**
=========================================================================================================================================

- https://www.ici-grenoble.org/agenda
- https://38.demosphere.net/
- :ref:`argumentaires`


#Grenoble #64AnsCestToujoursNon #ReformedesRetraites #AG


**Agenda ici-grenoble**
==========================

.. figure:: images/ici_grenoble_2023_04_28.png
   :align: center

   https://www.ici-grenoble.org/agenda

- 13h00 https://www.ici-grenoble.org/evenement/atelier-apprendre-a-creer-des-cyanotypes-de-plantes-ideal-pour-des-cartes-postales-diy
- 14h00 🎥  🇮🇷 https://www.ici-grenoble.org/evenement/documentaire-choc-sept-hivers-a-teheran-sur-les-droits-des-femmes-en-iran
- 15h00 |car38| https://www.ici-grenoble.org/evenement/theatre-forum-se-defendre-face-a-la-repression-policiere-judiciaire-et-administrative

      - https://www.ici-grenoble.org/article/15aine-anti-repression-le-programme

- |important| 17h00 https://www.ici-grenoble.org/evenement/ag-interprofessionnelle-contre-la-reforme-des-retraites
- 18h30 |car38| https://www.ici-grenoble.org/evenement/soiree-de-reflexions-autour-de-la-justice-transformatrice
- 19h00 https://www.ici-grenoble.org/evenement/festibaf-repas-vegan-et-concerts-pour-la-fin-de-la-baf
- 19h35 🎥 https://www.ici-grenoble.org/evenement/cinema-relaxe-sur-laffaire-tarnac
- 20h50 🎥 https://www.ici-grenoble.org/evenement/cinema-sur-ladamant-sur-la-psychiatrie-alternative
- 21h50 🎥 https://www.ici-grenoble.org/evenement/cinema-je-verrai-toujours-vos-visages-sur-les-experiences-de-justice-reparative
- 22h00 🎥 https://www.ici-grenoble.org/evenement/cinema-letabli-sur-lentrisme-revolutionnaire-dans-les-usines

Demosphere 38
=================

.. figure:: images/demo_2023_04_28.png
   :align: center

   https://38.demosphere.net/

- 15h00 |car38| https://38.demosphere.net/rv/1043 (ATELIER INITIATION THEATRE FORUM)

      - https://www.ici-grenoble.org/article/15aine-anti-repression-le-programme

- 17h00 https://38.demosphere.net/rv/1139 (Assemblée générale interprofessionnelle de Grenoble , Ecole sup d'art de Grenoble 25 Rue Lesdiguières)
- 18h30 |car38| https://38.demosphere.net/rv/1044 (REFLEXIONS AUTOUR DE LA JUSTICE TRANSFORMATRICE 2/2)

|jo| Informations sur 100 jours de zbeul
===========================================

- :ref:`100jours_zbeul`
- :ref:`100jours_de_zbeul_jour_11`

