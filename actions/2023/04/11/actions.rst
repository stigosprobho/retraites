
.. _actions_2023_04_11:

=========================================================================================================================================
**Actions du mardi 11 avril 2023**
=========================================================================================================================================

- https://www.mediamanif.org/
- https://www.ici-grenoble.org/agenda
- https://38.demosphere.net/
- :ref:`argumentaires`


#64AnsCestToujoursNon #ReformedesRetraite #Grenoble #MediaManif


**Agenda ici-grenoble**
==========================

- https://www.ici-grenoble.org/article/grevue-de-presse-les-10-infos-de-mars

.. figure:: images/ici_grenoble_2023_04_11.png
   :align: center

   https://www.ici-grenoble.org/agenda

- https://www.ici-grenoble.org/evenement/permanence-de-l-accorderie-de-grenoble
- https://www.ici-grenoble.org/evenement/permanence-du-rusf-isere-aide-a-la-reprise-detude-soutien-administratif
- https://www.ici-grenoble.org/evenement/repas-sur-place-ou-a-emporter-proposes-par-cuisine-sans-frontieres
- https://www.ici-grenoble.org/evenement/permanence-du-dal-isere-pour-tout-probleme-de-logement-en-tant-que-locataires
- https://www.ici-grenoble.org/evenement/assemblee-generale-de-lutte-contre-la-reforme-des-retraites
- https://www.ici-grenoble.org/evenement/cinema-letabli-sur-lentrisme-revolutionnaire-dans-les-usines
- https://www.ici-grenoble.org/evenement/documentaire-choc-sept-hivers-a-teheran-sur-les-droits-des-femmes-en-iran
- https://www.ici-grenoble.org/evenement/scene-ouverte-slam-rap-pour-meufs-trans-non-binaires
- https://www.ici-grenoble.org/evenement/replay-manifs-la-guerre-est-declaree-sur-les-violences-policieres-a-sainte-soline
- https://www.ici-grenoble.org/evenement/scene-ouverte-slam-rap-pour-meufs-trans-non-binaires
- https://www.ici-grenoble.org/evenement/coup-de-coeur-les-coulisses-de-la-revue-la-hulotte


.. _ag_luttes_2023_04_11:

Assemblée générale de luttes le mardi avril 2023
====================================================

- https://www.ici-grenoble.org/evenement/assemblee-generale-de-lutte-contre-la-reforme-des-retraites
- https://cric-grenoble.info/infos-locales/info-locales/article/ag-de-luttes-2942

.. figure:: images/ag_de_luttes_2023_04_11.png
   :align: center


Demosphere 38
=================

- https://38.demosphere.net/

.. figure:: images/demo_2023_semaine_15.png
   :align: center

   https://38.demosphere.net/

.. figure:: images/demo_2023_04_11.png
   :align: center

   https://38.demosphere.net/

