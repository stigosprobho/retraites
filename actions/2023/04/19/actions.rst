
.. _actions_2023_04_19:

=========================================================================================================================================
**Actions du mercredi 19 avril 2023 #Grenoble #64AnsCestToujoursNon #ReformedesRetraites**
=========================================================================================================================================


- https://www.ici-grenoble.org/agenda
- https://38.demosphere.net/
- :ref:`argumentaires`


#Grenoble #64AnsCestToujoursNon #ReformedesRetraites


**Agenda ici-grenoble**
==========================

.. figure:: images/ici_grenoble_2023_04_19.png
   :align: center

   https://www.ici-grenoble.org/agenda

- https://www.ici-grenoble.org/evenement/ag-des-etudiant-e-s-en-lutte-contre-la-reforme-des-retraites
- https://www.ici-grenoble.org/evenement/cine-debat-antifasciste-pinocchio-sur-letat-le-travail-la-famille-et-la-religion
- https://www.ici-grenoble.org/evenement/permanence-du-magasin-gratuit-du-38-rue-dalembert-don-de-vetements-aliments-objets
- https://www.ici-grenoble.org/evenement/conference-une-histoire-critique-du-maintien-de-lordre-et-des-violences-policieres-en-france
- https://www.ici-grenoble.org/evenement/atelier-de-danse-moderne-ouvert-a-tous-tes
- https://www.ici-grenoble.org/evenement/cinema-letabli-sur-lentrisme-revolutionnaire-dans-les-usines
- https://www.ici-grenoble.org/evenement/reunion-de-quartier-saint-bruno-organisons-la-lutte-contre-la-reforme-des-retraites
- https://www.ici-grenoble.org/evenement/soiree-de-soutien-aux-postier-e-s-de-grenoble-en-lutte
- https://www.ici-grenoble.org/evenement/cinema-je-verrai-toujours-vos-visages-sur-les-experiences-de-justice-reparative
- https://www.ici-grenoble.org/evenement/coup-de-coeur-quest-ce-qui-pourrait-sauver-lamour-nouveau-podcast-dovidie


Demosphere 38
=================

.. figure:: images/demo_2023_04_19.png
   :align: center

   https://38.demosphere.net/

Evénement historique : les 80 ans du soulèvement du ghetto de Varsovie le 19 avril 1943
===========================================================================================

- https://fr.wikipedia.org/wiki/Soul%C3%A8vement_du_ghetto_de_Varsovie
- https://www.aacce.fr/2023/04/80-ans-du-soulevement-du-ghetto-de-varsovie.html
- https://www.grenoble.fr/agendaSimple/89914/1995-conference-et-chroniques-du-ghetto-de-varsovie.htm

Le soulèvement du ghetto de Varsovie est une révolte armée, organisée et
menée par la population juive du ghetto de Varsovie contre les forces
d'occupation allemandes entre le 19 avril et le 16 mai 1943.
C'est l'acte de résistance juive pendant la Shoah le plus connu et le
plus commémoré.
