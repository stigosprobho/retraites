
.. _actions_2023_04_12:

=========================================================================================================================================
**Actions du mercredi 12 avril 2023**
=========================================================================================================================================

- https://www.mediamanif.org/
- https://www.ici-grenoble.org/agenda
- https://38.demosphere.net/
- :ref:`argumentaires`


#64AnsCestToujoursNon #ReformedesRetraite #Grenoble #MediaManif


**Agenda ici-grenoble**
==========================

- https://www.ici-grenoble.org/article/grevue-de-presse-les-10-infos-de-mars

.. figure:: images/ici_grenoble_2023_04_12.png
   :align: center

   https://www.ici-grenoble.org/agenda

- https://www.ici-grenoble.org/evenement/le-12-avril-1803-l-instauration-du-livret-ouvrier-premier-pas-vers-la-carte-d-identite
- https://www.ici-grenoble.org/evenement/permanence-du-magasin-gratuit-du-38-rue-dalembert-don-de-vetements-aliments-objets
- https://www.ici-grenoble.org/evenement/atelier-de-danse-moderne-ouvert-a-tous-tes
- https://www.ici-grenoble.org/evenement/ag-ecolo-anticapitaliste-1 (**erreur 500 le 2023-04-12 à 15h30**)
- https://www.ici-grenoble.org/evenement/cercle-de-lectures-feministes-de-grenoble
- https://www.ici-grenoble.org/evenement/cine-caisse-de-greve-la-sociale-venon
- https://www.ici-grenoble.org/evenement/coup-de-coeur-comment-reussir-la-convergence-des-luttes-malgre-les-desaccords-politiques-les-lecons-de-la-resistance-francaise-en-39-45

Demosphere 38
=================

- https://38.demosphere.net/

.. figure:: ../11/images/demo_2023_semaine_15.png
   :align: center

   https://38.demosphere.net/

.. figure:: images/demo_2023_04_12.png
   :align: center

   https://38.demosphere.net/

