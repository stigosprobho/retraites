
.. _actions_2023_04_22:

=========================================================================================================================================
**Actions du samedi 22 avril 2023 #Grenoble #64AnsCestToujoursNon #ReformedesRetraites**
=========================================================================================================================================

- https://www.ici-grenoble.org/agenda
- https://38.demosphere.net/
- :ref:`argumentaires`


#Grenoble #64AnsCestToujoursNon #ReformedesRetraites


**Agenda ici-grenoble**
==========================

.. figure:: images/ici_grenoble_2023_04_22.png
   :align: center

   https://www.ici-grenoble.org/agenda


- https://www.ici-grenoble.org/evenement/atelier-patriarchie-initiation-au-bootyshake-expose-sur-lhypersexualisation-des-fesses-des-femmes-noires
- https://www.ici-grenoble.org/evenement/formation-comprendre-et-minimiser-la-surveillance-numerique-tout-niveau
- https://www.ici-grenoble.org/evenement/rassemblement-pour-larret-des-executions-en-iran-et-contre-les-attaques-chimiques-dans-les-ecoles
- https://www.ici-grenoble.org/evenement/manifestation-de-soutien-au-peuple-ukrainien
- https://www.ici-grenoble.org/evenement/grande-velorution-de-grenoble-venez-deguise-e-s
- https://www.ici-grenoble.org/evenement/rencontres-la-gratuite-des-transports-en-communs-a-grenoble-pourquoi-et-comment
- https://www.ici-grenoble.org/evenement/90-8-fm-nuit-du-mix-sets-dj-en-non-mixite-choisie-sans-mecs-cis
- https://www.ici-grenoble.org/evenement/repas-vegan-et-concert-punk-a-la-baf
- https://www.ici-grenoble.org/evenement/coup-de-coeur-les-coulisses-de-la-revue-la-hulotte


Demosphere 38
=================

.. figure:: images/demo_2023_04_22.png
   :align: center

   https://38.demosphere.net/

- https://38.demosphere.net/rv/1034 (Culture de la sécurité - comprendre et minimiser la surveillance numérique)
- https://38.demosphere.net/rv/1072 (Concert de résistance Châtel-en-Trièves)

|car38| Culture de la sécurité - comprendre et minimiser la surveillance numérique
--------------------------------------------------------------------------------------------------

- https://38.demosphere.net/rv/1034
- https://www.ici-grenoble.org/structure/le-collectif-anti-repression-isere
- https://www.ici-grenoble.org/article/la-quinzaine-anti-repression


En lien avec la quinzaine anti-répression organisée par le car38
(collectif anti-répression de Grenoble), on propose une formation sur
les communications et le numérique.

De la perquisition, des mises sous écoute téléphonique, des demandes des
sites internet que l'on fréquente auprès des fournisseurs d'accès internet,
ou de récupérer nos courriel auprès de nos boites mails, toutes ces pratiques
policières sont des infiltrations dans nos vis privées pour nous humilier,
réprimer et saper nos luttes.

De manière générale le vol de données personnelles par les grandes
entreprises du numérique est un vrai problème.

Le temps se fera en plusieurs parties (prévoir 4h30 environ avec les pauses
pour participer à tout), ça abordera en fonction des envies la culture
de sécurité dans nos pratiques de luttes, des compréhensions d'outils
de surveillance policière vis à vis des ordis / téléphones, ce qui peut
être mis en œuvre pour s'en protéger, pour les ordis expérimenter
les clés « TAILS » qui sert à pratiquer facilement des outils de
sécurité numérique.

Tous niveaux, venez avec vos questions, avec ou sans ordinateurs, avec
ou sans clé USB.

Concert de résistance Châtel-en-Trièves
----------------------------------------------

- https://38.demosphere.net/rv/1072

La mobilisation et la lutte contre la réforme des retraites continue !

Au programme : concerts tous publics, buvette, pizzas au feu de bois.

- Du rock'n'Roll avec Whisky Of Blood > https://www.youtube.com/watch?v=737ggTSWpV8
- De la chanson avec Ste Rita & Nina la Démone
- Du mix et bien d'autres encore

Prix libre.

Bénéfices reversés à la caisse de grève.
