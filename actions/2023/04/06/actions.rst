
.. _actions_2023_04_06:

=========================================================================================================================================
**Actions du jeudi 6 avril 2023, 11e manifestation appel intersyndical à bloquer le pays contre une réforme des retraites injuste**
=========================================================================================================================================

- https://www.mediamanif.org/
- https://www.ici-grenoble.org/agenda
- https://38.demosphere.net/
- :ref:`argumentaires`


#64AnsCestToujoursNon #ReformedesRetraite #Grenoble #MediaManif


**Agenda ici-grenoble**
==========================

- https://www.ici-grenoble.org/article/le-jeudi-des-tenaces
- https://www.ici-grenoble.org/article/grevue-de-presse-les-10-infos-de-mars

.. figure:: images/ici_grenoble_2023_04_06.png
   :align: center

   https://www.ici-grenoble.org/agenda


- https://www.ici-grenoble.org/article/le-jeudi-des-tenaces
- https://www.ici-grenoble.org/evenement/appel-intersyndical-a-bloquer-le-pays-contre-une-reforme-des-retraites-injuste
- https://www.ici-grenoble.org/evenement/chantier-collectif-prenons-les-terres-dans-lagglo-grenobloise
- https://www.ici-grenoble.org/evenement/atelier-couture-aux-petites-cantines
- https://www.ici-grenoble.org/evenement/manifestation-intersyndicale-contre-la-reforme-des-retraites
- https://www.ici-grenoble.org/evenement/permanence-dautodefense-pour-lacces-aux-droits-caf-secu-titres-de-sejour-dossiers-a-remplir
- https://www.ici-grenoble.org/evenement/permanence-du-dal-isere-pour-tout-probleme-de-logement
- https://www.ici-grenoble.org/evenement/theatre-forum-les-violences-sexistes-et-les-inegalites-de-genre
- https://www.ici-grenoble.org/evenement/aperotiba-soiree-de-presentation-dalternatiba-grenoble
- https://www.ici-grenoble.org/evenement/atelier-education-populaire-lectures-collectives-sur-les-rapports-de-genre-au-travail



Demosphere 38
=================

.. figure:: images/demo_2023_04_06.png
   :align: center

   https://38.demosphere.net/


Compte rendus
===============

- https://www.ici-grenoble.org/article/merci-aux-revolte-e-s

Malgré les contre-feux médiatiques, la lutte contre la réforme des retraites
continue : 10 000 à 20 000 manifestant-e-s jeudi dans les rues de Grenoble,
et une prochaine manifestation le 13 avril 2023.

Dans ce contexte politique si sombre, face à un capitalisme de plus en
plus autoritaire, mensonger et répressif, face à l'extrême-droite en
embuscade, merci à celles et ceux qui résistent, qui manifestent, qui
s'organisent, qui bloquent, qui prennent des risques.

Les vacances scolaires démarrent, le mouvement social va donc "mécaniquement"
reculer. Les luttes vont-elles repartir de plus belle dans deux semaines ?
