
.. _actions_2023_04_03:

=======================================================================================================================
**Actions du lundi 3 avril 2023**
=======================================================================================================================

- https://www.mediamanif.org/
- https://www.ici-grenoble.org/agenda
- https://38.demosphere.net/
- :ref:`argumentaires`


#64AnsCestToujoursNon #ReformedesRetraite #Grenoble #MediaManif


**Agenda ici-grenoble**
==========================

- https://www.ici-grenoble.org/article/grevue-de-presse-les-10-infos-de-mars

.. figure:: images/ici_grenoble_2023_04_03.png
   :align: center

   https://www.ici-grenoble.org/agenda


- https://www.ici-grenoble.org/evenement/chantier-collectif-prenons-les-terres-dans-lagglo-grenobloise
- https://www.ici-grenoble.org/evenement/distribution-de-repas-gratuits-pour-les-personnes-les-plus-demunies
- https://www.ici-grenoble.org/evenement/permanence-du-garage-associatif-les-soupapes-reparer-sa-voiture
- https://www.ici-grenoble.org/evenement/temps-dechange-sur-le-sexisme-dans-la-lutte-sur-le-campus
- https://www.ici-grenoble.org/evenement/documentaire-choc-sept-hivers-a-teheran-sur-les-droits-des-femmes-en-iran
- https://www.ici-grenoble.org/evenement/formation-anti-repression-que-faire-face-aux-violences-de-la-police-et-aux-arrestations
- https://www.ici-grenoble.org/evenement/atelier-de-clown-activisme
- https://www.ici-grenoble.org/evenement/repetition-ouverte-de-la-chorale-autogeree-la-cagette
- https://www.ici-grenoble.org/evenement/soiree-enjeux-communs
- https://www.ici-grenoble.org/evenement/table-ronde-face-a-lextreme-droite-le-racisme-et-limperialisme-francais-comment-lutter
- https://www.ici-grenoble.org/evenement/cine-rencontres-tu-nourriras-le-monde-une-reflexion-critique-sur-lagriculture-cerealiere-industrielle
- https://www.ici-grenoble.org/evenement/cinemarc-en-ciel-les-invisibles-sur-les-personnes-lgbtqia-de-75-a-85-ans
- https://www.ici-grenoble.org/evenement/temps-dechange-sur-le-sexisme-dans-la-lutte-sur-le-campus

CRIC
=====

- https://cric-grenoble.info/infos-locales/article/formation-antirepression-avancee-2927


Demosphere 38
=================

.. figure:: images/demo_2023_04_03.png
   :align: center

   https://38.demosphere.net/

