
.. _actions_2023_04_08:

=========================================================================================================================================
**Actions du samedi 8 avril 2023**
=========================================================================================================================================

- https://www.mediamanif.org/
- https://www.ici-grenoble.org/agenda
- https://38.demosphere.net/
- :ref:`argumentaires`

#64AnsCestToujoursNon #ReformedesRetraite #Grenoble #MediaManif


**Agenda ici-grenoble**
==========================

- https://www.ici-grenoble.org/article/grevue-de-presse-les-10-infos-de-mars

.. figure:: images/ici_grenoble_2023_04_08.png
   :align: center

   https://www.ici-grenoble.org/agenda

- https://www.ici-grenoble.org/evenement/concerts-le-developpement-du-rap
- https://www.ici-grenoble.org/evenement/manifestation-de-soutien-au-peuple-ukrainien
- https://www.ici-grenoble.org/evenement/discussion-le-sens-politique-du-travail
- https://www.ici-grenoble.org/evenement/rencontre-avec-les-collectifs-ancrage-et-la-fonciere-antidote-comment-perenniser-des-lieux-autogeres
- https://www.ici-grenoble.org/evenement/repas-vegan-et-concert-de-soutien-a-kontrosol-centre-social-autogere-lgbtqi-en-grece
- https://www.ici-grenoble.org/evenement/soiree-rap-hip-hop-ferme-de-loutas
- https://www.ici-grenoble.org/evenement/replay-choc-manifs-la-guerre-est-declaree-sur-les-violences-policieres-a-sainte-soline
- https://www.francetvinfo.fr/replay-magazine/france-2/complement-d-enquete/complement-d-enquete-du-jeudi-6-avril-2023-manifs-la-guerre-est-declaree_5745668.html
- :ref:`iran_luttes:iran_grenoble_2023_04_08`


- :ref:`actions_2023_04_01`


