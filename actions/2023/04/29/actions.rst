
.. _actions_2023_04_29:

=========================================================================================================================================
**Actions du samedi 29 avril 2023 #Grenoble #64AnsCestToujoursNon #ReformedesRetraites**
=========================================================================================================================================

- https://www.ici-grenoble.org/agenda
- https://38.demosphere.net/
- :ref:`argumentaires`


#Grenoble #64AnsCestToujoursNon #ReformedesRetraites


**Agenda ici-grenoble**
==========================

.. figure:: images/ici_grenoble_2023_04_29.png
   :align: center

   https://www.ici-grenoble.org/agenda

- https://www.ici-grenoble.org/evenement/formation-construire-et-repartir-avec-une-parabole-solaire-cuisson-rapide-des-aliments
- 9h30: https://www.ici-grenoble.org/evenement/stage-dautodefense-pour-femmes-cis-et-trans-personnes-non-binaires-et-intersexes
- |important| 10h00 https://www.ici-grenoble.org/evenement/assemblee-generale-feministe-de-grenoble-en-mixite-choisie-sans-hommes-cisgenres
- 14h00 https://www.ici-grenoble.org/evenement/boom-boom-en-doudoune-2e-edition
- |important| 14h00 https://www.ici-grenoble.org/evenement/manifestation-contre-les-lois-darmanin-et-kasbarian-criminalisation-des-sans-abris-des-migrant-e-s-des-squats-et-locataires-en-difficulte
- 14h30: https://www.ici-grenoble.org/evenement/rencontre-avec-lautrice-de-beaufs-et-barbares-comment-detruire-letat-bourgeois-blanc-et-raciste
- |important| 15h00 https://www.ici-grenoble.org/evenement/manifestation-de-soutien-au-peuple-ukrainien
- 15h30 🎥: https://www.ici-grenoble.org/evenement/cine-rencontres-gouttes-dhuile-sur-loccupation-despaces-vacants-a-grenoble
- 18h00 ⚖️ https://www.ici-grenoble.org/evenement/atelier-juridique-comment-occuper-des-espaces-vacants-a-grenoble
- 19h00 https://www.ici-grenoble.org/evenement/festibaf-repas-vegan-et-concerts-pour-la-fin-de-la-baf
- 19h00 🎥 🇮🇷 https://www.ici-grenoble.org/evenement/documentaire-choc-sept-hivers-a-teheran-sur-les-droits-des-femmes-en-iran
- 20h00 https://www.ici-grenoble.org/evenement/fete-des-20-ans-des-passeurs-de-grenoble-occupation-despaces-vacants




Demosphere 38
=================

.. figure:: images/demo_2023_04_29.png
   :align: center

   https://38.demosphere.net/

- 10h: |important| https://38.demosphere.net/rv/1111 (AG féministe)
- 11h00 https://38.demosphere.net/rv/1065 (AG DU GROUPE DES SALARIÉ.E.S ET HABITANT.E.S DES QUARTIERS VILLENEUVE/MALHERBE/V.O/VIGNY-MUSSET)
- 14h00 |important| |important| https://38.demosphere.net/rv/1113 (Manifestation contre les lois Darmanin et Kasbarian )
