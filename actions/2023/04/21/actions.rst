
.. _actions_2023_04_21:

=========================================================================================================================================
**Actions du vendredi 21 avril 2023 #Grenoble #64AnsCestToujoursNon #ReformedesRetraites** #DroitDeVote #CPE #Occupation
=========================================================================================================================================

- https://www.ici-grenoble.org/agenda
- https://38.demosphere.net/
- :ref:`argumentaires`


#Grenoble #64AnsCestToujoursNon #ReformedesRetraites


**Agenda ici-grenoble**
==========================

.. figure:: images/ici_grenoble_2023_04_21.png
   :align: center

   https://www.ici-grenoble.org/agenda


- https://www.ici-grenoble.org/evenement/le-21-avril-1944-le-droit-de-vote-pour-les-femmes-en-france
- https://www.ici-grenoble.org/evenement/le-21-avril-2006-l-abrogation-du-cpe
- https://fr.wikipedia.org/wiki/Contrat_premi%C3%A8re_embauche
- https://www.ici-grenoble.org/evenement/les-agites-atelier-de-theatre-de-lopprime
- https://www.ici-grenoble.org/evenement/soiree-jeux-et-grignotage-vegan-a-la-b-a-f
- https://www.ici-grenoble.org/evenement/cinema-letabli-sur-lentrisme-revolutionnaire-dans-les-usines
- https://www.ici-grenoble.org/evenement/coup-de-coeur-rencontre-intime-et-politique-avec-virginie-despentes


Droit de vote pour les femmes en France
------------------------------------------

- https://www.ici-grenoble.org/evenement/le-21-avril-1944-le-droit-de-vote-pour-les-femmes-en-france
- https://fr.wikipedia.org/wiki/Droit_de_vote_des_femmes#En_France

Le 21 avril 1944, le droit de vote est accordé aux femmes en France par
une ordonnance du Comité français de la Libération nationale, signée par
Charles de Gaulle depuis Alger.

C'est le résultat de luttes féministes de longues haleines et d'intenses
débats au sein des mouvements de Résistance Français.

Le droit de vote des femmes sera effectif le 29 avril 1945, pour les
élections municipales, puis en octobre pour les élections à l’Assemblée constituante.

CPE
----

- https://fr.wikipedia.org/wiki/Contrat_premi%C3%A8re_embauche

En France, le contrat première embauche (CPE) était un type de contrat
de travail à durée indéterminée, à destination des moins de 26 ans prévu
par l'article 8 de la loi pour l'égalité des chances.

La vive opposition au projet a fait reculer le pouvoir exécutif.

Si la loi mettant en place le CPE a été publiée au journal officiel le
2 avril 2006, avec la promesse de Jacques Chirac que des modifications
seraient effectuées, un projet de loi présenté par le Premier ministre
du 10 avril 2006 a proposé de le retirer et de le remplacer par un
dispositif visant à favoriser l'insertion professionnelle des jeunes en difficulté.

L'article 8 de la loi 2006-396 du 31 mars 2006 a été abrogé par la loi
no 2006-457 du **21 avril 2006** sur l'accès des jeunes à la vie active en
entreprise. Le reste de la loi pour l'égalité des chances a été conservé.



Demosphere 38
=================

.. figure:: images/demo_2023_04_21.png
   :align: center

   https://38.demosphere.net/


- https://38.demosphere.net/rv/1094
- :ref:`communique_msh_2023_04_20`
