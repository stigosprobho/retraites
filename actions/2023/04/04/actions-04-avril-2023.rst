
.. _actions_2023_04_04:

=========================================================================================================================================
**Actions du mardi 4 avril 2023**
=========================================================================================================================================

- https://www.mediamanif.org/
- https://www.ici-grenoble.org/agenda
- https://38.demosphere.net/
- :ref:`argumentaires`


#64AnsCestToujoursNon #ReformedesRetraite #Grenoble #MediaManif


**Agenda ici-grenoble**
==========================

- https://www.ici-grenoble.org/article/grevue-de-presse-les-10-infos-de-mars

.. figure:: images/ici_grenoble_2023_04_04.png
   :align: center

   https://www.ici-grenoble.org/agenda


- https://www.ici-grenoble.org/evenement/operation-ville-morte-blocages-et-tractages-filtrants-sur-la-rocade-et-aux-entrees-de-grenoble
- https://www.ici-grenoble.org/evenement/permanence-du-rusf-isere-aide-a-la-reprise-detude-soutien-administratif
- https://www.ici-grenoble.org/evenement/repas-sur-place-ou-a-emporter-proposes-par-cuisine-sans-frontieres
- https://www.ici-grenoble.org/evenement/conference-une-analyse-critique-de-la-reforme-des-retraites
- https://www.ici-grenoble.org/evenement/permanence-du-dal-isere-pour-tout-probleme-de-logement-en-tant-que-locataires
- https://www.ici-grenoble.org/evenement/vernissage-exposition-nous-occupons
- https://www.ici-grenoble.org/evenement/table-ronde-le-film-marvel-black-panther-hymne-a-lafrique-ou-appropriation-culturelle
- https://www.ici-grenoble.org/evenement/assemblee-generale-de-lutte-contre-la-reforme-des-retraites
- https://www.ici-grenoble.org/evenement/cine-rencontres-lincrevable-tour-sur-le-cyclo-feminisme
- https://www.ici-grenoble.org/evenement/rencontres-feministes-autour-du-roman-la-nuit-des-beguines-sur-des-heroines-subversives


Demosphere 38
=================

.. figure:: images/demo_2023_04_04.png
   :align: center

   https://38.demosphere.net/

