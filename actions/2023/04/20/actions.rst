
.. _actions_2023_04_20:

=========================================================================================================================================
**Actions du jeudi 20 avril 2023 AGs, Manifestations et occupations #Grenoble #64AnsCestToujoursNon #ReformedesRetraites**
=========================================================================================================================================

- https://www.ici-grenoble.org/agenda
- https://38.demosphere.net/
- :ref:`argumentaires`


AGs, Manifestations et occupations sur #Grenoble #64AnsCestToujoursNon #ReformedesRetraites


**Agenda ici-grenoble**
==========================

.. figure:: images/ici_grenoble_2023_04_20.png
   :align: center

   https://www.ici-grenoble.org/agenda

- https://www.ici-grenoble.org/evenement/distribution-de-repas-gratuits-pour-les-personnes-les-plus-demunies
- https://www.ici-grenoble.org/evenement/permanence-de-l-accorderie-de-grenoble
- https://www.ici-grenoble.org/evenement/repas-sur-place-ou-a-emporter-proposes-par-cuisine-sans-frontieres
- https://www.ici-grenoble.org/evenement/documentaire-choc-sept-hivers-a-teheran-sur-les-droits-des-femmes-en-iran
- https://www.ici-grenoble.org/evenement/rassemblement-de-soutien-aux-postier-e-s-de-grenoble-en-lutte
- https://www.ici-grenoble.org/evenement/permanence-dautodefense-pour-lacces-aux-droits-caf-secu-titres-de-sejour-dossiers-a-remplir
- https://www.ici-grenoble.org/evenement/permanence-du-dal-isere-pour-tout-probleme-de-logement
- https://www.ici-grenoble.org/evenement/ag-interprofessionnelle-contre-la-reforme-des-retraites
- https://www.ici-grenoble.org/evenement/cine-debat-antifasciste-regarde-ailleurs-sur-limmigration-la-police-et-les-frontieres
- https://www.ici-grenoble.org/evenement/atelier-la-fresque-de-la-biodiversite-sur-lextinction-du-vivant
- https://www.ici-grenoble.org/evenement/cinema-letabli-sur-lentrisme-revolutionnaire-dans-les-usines
- https://www.ici-grenoble.org/evenement/cinema-je-verrai-toujours-vos-visages-sur-les-experiences-de-justice-reparative

Demosphere 38
=================

.. figure:: images/demo_2023_04_20.png
   :align: center

   https://38.demosphere.net/

Rassemblement à 12h contre la réforme des retraites devant le bâtiment de la présidence de l'université (621 avenue centrale, Saint Martin d'Hères)
--------------------------------------------------------------------------------------------------------------------------------------------------------

- https://38.demosphere.net/rv/1089
- https://www.openstreetmap.org/#map=19/45.19146/5.76803


Rassemblement devant le bâtiment de la présidence de l'université
(621 avenue centrale, Saint Martin d'Hères) à l'initiative de l'AG des
personnels de l'UGA contre la réforme des retraites.


Rassemblement 14h en soutien aux postiers victimes de répression syndicale de la part de leur direction 88 avenue Rhin et Danube
-------------------------------------------------------------------------------------------------------------------------------------

- https://38.demosphere.net/rv/1081
- https://www.openstreetmap.org/#map=19/45.16842/5.70311
- https://www.ici-grenoble.org/evenement/rassemblement-de-soutien-aux-postier-e-s-de-grenoble-en-lutte


AG interpro aujourd'hui à 17h à la Bobine à Grenoble
---------------------------------------------------------------

- https://38.demosphere.net/rv/1080
- https://www.ici-grenoble.org/evenement/ag-interprofessionnelle-contre-la-reforme-des-retraites
- https://www.openstreetmap.org/#map=19/45.18286/5.73627


::

    42 boulevard Clémenceau
    38000 Grenoble
    04 76 70 37 58


Pour rappel : AG interpro aujourd'hui à 17h à la Bobine à Grenoble :

AG ouverte à toutes et tous pour faire le point sur les différents secteurs
pro en lutte, discuter des suites à donner à la mobilisation pour les prochaines
semaines et réfléchir à des actions concrètes.


2023-04-19 Occupation de bâtiment maison des sciences de l'Homme par l'AG étudiante
=============================================================================================

- https://38.demosphere.net/rv/1090
- https://www.msh-alpes.fr/
- https://www.openstreetmap.org/#map=19/45.19188/5.77355

::

    Maison des Sciences de l'Homme-Alpes
    1221 avenue centrale - Domaine universitaire
    38400 Saint-Martin-d'Hères
    +33 (0)4 76 01 26 45




Objectifs
---------------

- Créer un espace de lutte pour organiser la mobilisation étudiante grenobloise,
- informer les étudiant.e.s de la situation en cours,
- organiser des échanges avec des intervenant.e.s,
- proposer un espace d'échange à participation collective.

Les revendications portés par l'occupation:

- Politisation et formation anti-répression accessible à tou.te.s les
  étudiant.e.s (à préciser en AG pendant l'occupation pour les personnes non étudiantes)
- Plus de lieux autogérés sur le campus par les étudiant.e.s
- Lieu ouvert à tou.te.s (à préciser en AG pendant l'occupation pour les
  personnes non étudiantes) pour discuter, participer à la vie du lieu et aux activités
- Éducation sociale, écologique, politique et autogestionnaire
- Un lieu pour s'organiser pour les actions de l'Assemblée générale étudiante H24

Les revendications portés au gouvernement
--------------------------------------------

- La suppression de la réforme des retraites, de la loi Darmanin ainsi
  que la loi Kasbarian Berger.

La nécessité de réelle prise en considération :

- De la précarité étudiante
- Des inégalités et violences raciste sexiste homophobe et transphobe
- De la crise environnementale
- Le respect de la volonté de la majorité de la population française et
  d'une réelle démocratie.

Les revendication portés à la présidence et aux directions de l'université :

- La banalisation des cours ou la levée d'assiduité pour tou.te.s les étudiant.e.s,
  lors des journée de manifestation d'ordre nationale, ainsi que celle organisée
  dans le cas d'une grève reconductible.
- L'interdiction de l'usage du distanciel (appliquée), qui devient un outil
  casseur de grève et de mobilisation.
- Un aménagement des partiels pour ne pas traiter les notions non abordées
  à cause des cours banalisés.
- Un soutien pour appeler à la mobilisation des étudiant.e.s et personnel.le.s via :

Le non retrait des collages d'information et des banderoles (non ostentatoires
et en lien avec la mobilisation).

L'accès aux mails étudiant.e.s pour diffuser des informations de l'avancé
du mouvement et notamment les dates de manifestations et d'AG étudiant.e.s.

Règlement
-------------

Organiser une AG chaque jour (puis suivant les besoins) pour déterminer
collectivement la suite de l'occupation, seul.e les étudiant.es sont
légitimes pour voter les décisions.

- Ne pas dégrader les locaux et le matériel.
- Ne pas consommer de drogue ou d'alcool dans l'enceinte de l'occupation. !VSS!
- Ne pas être dans un état second dans l'enceinte de l'occupation. !VSS!
- Respecter l'organisation des espaces genrées. !VSS!
- Garder l'espace propre.
- Ne pas porter d'arme dans l'enceinte de l'occupation.
- Ne pas user de violences verbales ou physiques envers les occupant.e.s. !VSS!
