
.. _actions_2023_04_13:

=========================================================================================================================================
**Actions du jeudi 13 avril 2023 12e manifestation contre la réforme des retraites**
=========================================================================================================================================

- https://www.ici-grenoble.org/article/le-13-avril-avec-tenacite
- https://www.ici-grenoble.org/agenda
- https://38.demosphere.net/
- :ref:`argumentaires`


#64AnsCestToujoursNon #ReformedesRetraite #Grenoble


**Agenda ici-grenoble**
==========================

- https://www.ici-grenoble.org/article/grevue-de-presse-les-10-infos-de-mars

.. figure:: images/ici_grenoble_2023_04_13.png
   :align: center

   https://www.ici-grenoble.org/agenda


- https://www.ici-grenoble.org/evenement/appel-intersyndical-a-bloquer-le-pays-contre-une-reforme-des-retraites-injuste
- https://www.ici-grenoble.org/evenement/permanence-de-l-accorderie-de-grenoble
- https://www.ici-grenoble.org/evenement/repas-sur-place-ou-a-emporter-proposes-par-cuisine-sans-frontieres
- https://www.ici-grenoble.org/evenement/manifestation-intersyndicale-contre-la-reforme-des-retraites
- https://www.ici-grenoble.org/evenement/permanence-pour-les-personnes-victimes-ou-temoins-de-discriminations-racistes-sexistes-lgbtqiphobes
- https://www.ici-grenoble.org/evenement/permanence-dautodefense-pour-lacces-aux-droits-caf-secu-titres-de-sejour-dossiers-a-remplir
- https://www.ici-grenoble.org/evenement/permanence-du-dal-isere-pour-tout-probleme-de-logement
- https://www.ici-grenoble.org/evenement/initiation-a-la-creation-daffiches-studio-photo-et-infographie
- https://www.ici-grenoble.org/evenement/coup-de-coeur-les-methodes-des-reseaux-sociaux-pour-manipuler-les-foules


Demosphere 38
=================

- https://38.demosphere.net/

.. figure:: ../11/images/demo_2023_semaine_15.png
   :align: center

   https://38.demosphere.net/

.. figure:: images/demo_2023_04_13.png
   :align: center

   https://38.demosphere.net/


Bal populaire des luttes à la Bobine de 19h à 22h
-------------------------------------------------------

- https://38.demosphere.net/rv/1037 (Bal populaire des luttes)

Avec la CGT culture 38 et l'AG culture en lutte on vous invite après la
manif à venir partager un moment festif et militant à la Bobine de 19h à 22h.

Soirée bal populaire des luttes avec prises de parole des secteurs en
lutte (Poste, Occelia, école d'archi, Planning Familial, énergie, dal,
beaux arts, culture, jeunesse...)

Entrée prix libre reversée aux caisses de grève Interpro et fédé spectacle.

Soyons nombreux.ses !

