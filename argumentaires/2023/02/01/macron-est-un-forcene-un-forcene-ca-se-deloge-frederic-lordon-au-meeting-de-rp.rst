.. index::
   pair: Argumentaire; Lordon

.. _lordon_2023_02_01:

=====================================================================================================
2023-02-01 **Macron est un forcené, un forcené ça se déloge** Frédéric Lordon au meeting de RP
=====================================================================================================

- https://www.ici-grenoble.org/article/des-blocages-aux-flambeaux
- https://www.youtube.com/watch?v=1FWpGfj9y5g

Comment gagner contre Macron et la réforme des retraites ?
L'intervention de Frédéric Lordon au meeting de RP le 1er février 2023.

Retrouvez le meeting en entier ici ▸    • MEETING DE RP : C...

Avec Anasse Kazib (cheminot, porte parole de RP), Frédéric Lordon (philosophe),
Fernande Bagou (travailleuse du nettoyage à Onet), Lorélia Fréjo (étudiante),
Mouloud Sahraoui (ouvrier dans la logistique à Geodis) et Adrien Cornet
(raffineur Total Grandpuits).


