.. index::
   pair: Argumentaire; les huit arguments fallacieux du gouvernement pour reculer l’âge de départ

.. _argumentaire_2023_01_02:

=======================================================================================================================
2023-01-02 **Réforme des retraites : les huit arguments fallacieux du gouvernement pour reculer l’âge de départ**
=======================================================================================================================

- https://www.ici-grenoble.org/article/des-blocages-aux-flambeaux
- https://basta.media/Reforme-des-retraites-les-huit-arguments-fallacieux-du-gouvernement-pour-reculer-l-age-de-depart

Ministres et membres de la majorité multiplient les déclarations chocs
pour tenter de convaincre de la nécessité d’une nouvelle réforme des retraites.

Basta! fait le tri entre arguments sérieux et ceux qui sont de mauvaise foi.

